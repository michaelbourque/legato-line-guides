<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use App\Subscribers;
use App\Exclusions;
use App\SugarBowl\SB_Submission;
use App\SugarBowl\SB_Location;
use App\SugarBowl\SB_User;

class limitController extends Controller
{
  /*
      This function
   */
  public function in_md_array($array, $key, $value)
  {
    $results = array();

    if (is_array($array)) {
      if (isset($array[$key]) && $array[$key] == $value) {
        $results[] = $array;
      }

      foreach ($array as $subarray) {
        $results = array_merge($results, Self::in_md_array($subarray, $key, $value));
      }
    }

    return $results;
  }

  public function getSubmissionData($submission)
  {
    // TODO Build this array:
    // $results = submission [ submission_name, submission_id, assigned_user, assigned_user_id, assigned_uw_level, risk_average],
    //                location [ location_number, location_name, location_id, building_number, construction_code, town_grade, TIV, industry_code_c, other_industry_code_c]
    //                policy [ policy_number ]

    $errors = false;
    $errormessage = '';


    $details = array();
    $policies = array();
    $locations = array();
    $locationdetails = array();

    $submissionresult = SB_Submission::where("name", "=", $submission)->where("deleted", "=", 0)->with("locations")->with("policies")->first();
    if (!$submissionresult) {
      return redirect()->route('nosubmission');
    }

    $userresult = SB_User::where("id", "=", $submissionresult->assigned_user_id)->where("deleted", "=", 0)->leftJoin("users_cstm", "users.id", "=", "users_cstm.id_c")->first();
    $details['submission_name'] = $submissionresult->name;
    $details['submission_id'] = $submissionresult->id;
    $details['assigned_user'] = $userresult->first_name . ' ' . $userresult->last_name;
    $details['assigned_user_id'] = $submissionresult->assigned_user_id;
    $details['assigned_uw_level'] = ($userresult->uwclass_c == null) ? '3' : $userresult->uwclass_c;
    $details['risk_average'] = $submissionresult->risk_average;

    foreach ($submissionresult->policies as $policy) {
      $policies[] = $policy->name;
    }

    $details['policies'] = $policies;
    $details['policycount'] = count($policies);

    foreach ($submissionresult->locations as $location) {
      $buildings[] = $location->id;
    }

    foreach ($buildings as $building) {

      $locationresult = SB_Location::where("id", "=", $building)->leftJoin("w887_property_rating_cstm", "w887_property_rating.id", "=", "w887_property_rating_cstm.id_c")->first();
      $locationdetails['location_number'] = $locationresult->location_number_c;
      $locationdetails['location_name'] = $locationresult->name;
      $locationdetails['location_id'] = $locationresult->id;
      $locationdetails['building_number'] = $locationresult->building_number_c;
      $locationdetails['construction_code'] = $locationresult->construction_code;
      $locationdetails['town_grade'] = $locationresult->town_grade;
      $locationdetails['tiv'] = $locationresult->tiv;
      $locationdetails['industry_code'] = str_pad($locationresult->industry_code_c, 4, "0", STR_PAD_LEFT);
      $locationdetails['other_code'] = str_pad($locationresult->other_industry_code_c, 4, "0", STR_PAD_LEFT);
      $locationdetails['province'] = $locationresult->gt_stateprovinces_code_c;
      $locations[] = $locationdetails;
    }
    $newLocations = array();

    foreach($locations as $newlocation) {

      $checkArray = Self::in_md_array($newLocations, 'location_number', $newlocation['location_number']);

      if (count($checkArray) > 0) {

            for ($i=0; $i < count($newLocations); $i++){
              if ($newLocations[$i]['location_number'] == $newlocation['location_number']) {
                $key = $i;
              }
            }

        $newLocations[$key]['tiv'] += $newlocation['tiv'];
        $newLocations[$key]['other_codes'][] = str_pad($newlocation['industry_code'], 4, "0", STR_PAD_LEFT);
        $newLocations[$key]['other_codes'][] = str_pad($newlocation['other_code'], 4, "0", STR_PAD_LEFT);
        $newLocations[$key]['construction_code'] = max($newLocations[$key]['construction_code'], $newlocation['construction_code']);

        if ($newLocations[$key]['town_grade'] != $newlocation['town_grade']) {
          $errors = true;
          $errormessage .= 'Town Grade mismatch error for Location ' . $newlocation['location_name'] . '. Please close this window, correct the Town Grade and try again. <br />';
        }
      } else {
        $newLocations[] = $newlocation;
      }

    }



    $details['locations'] = $newLocations;

    $details['disabled'] = 'DISABLED';
    $details['locationcount'] = count($locations);


    $error = array(
      'message' => $errormessage,
      'type' => 'danger',
      'class' => "fa fa-exclamation",
      'fatal' => true,
    );

    if ($errors) {
      $details['error'] = $error;
    }

    $details['subscribers'] = Self::getSubscriberList();

    return $details;

  }

  public function getLimitsBySubmission($submission)
  {
    // TODO return a list of subscribers and the limits based on criteria from the submission
    $submissionData = Self::getSubmissionData($submission);
    //dd($submissionData);

    if (isset($submissionData->headers)) {
      return $submissionData;
    } else {
      return view('lineguides')->with('data', $submissionData);
    }

    //return $submissionData['locations'];
  }

  public function getEmptyForm()
  {

    $users = SB_User::where('employee_status', '=', 'Active')->where('department', '=', 'Underwriting')->get();
    foreach($users as $user) {
      $uwclass = DB::connection('sugar')->table('users_cstm')->select('uwclass_c')->where('id_c', '=', $user->id)->first();
      if (isset($uwclass->uwclass_c)) {
        $class = $uwclass->uwclass_c;
      } else {
        $class = '3';
      }
      $underwriters[] = array($user->first_name . ' ' . $user->last_name, $class);
    }
    return view('lineguides_blank')->with('data', $underwriters);
  }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getLineGuide($subscriber = 'test', $riskgrade = 'average', $uwlevel='1', $towngrade='4', $buildinggrade='3', $province='ON', $iao = null, $other = null)
    {

        // Route::get('/{subscriber}/{riskgrade}/{uwlevel}/{towngrade}/{buildinggrade}', 'limitController@getLineGuide');

        $lineguide['subscriber'] = $subscriber;
        $lineguide['riskgrade'] = $riskgrade;
        $lineguide['uwlevel'] = $uwlevel;
        $lineguide['towngrade'] = $towngrade;
        $lineguide['buildinggrade'] = $buildinggrade;
        $lineguide['province'] = $province;
        $lineguide['iao'] = $iao;
        $lineguide['other'] = $other;

        $limitresult = Self::getLimit($lineguide);

        return $limitresult;
    }

    public function getExclusions($subscriber)
    {
      $subscriberdata = Subscribers::where('identifier', '=', $subscriber)->first();
      $results = Exclusions::where('subscriber_id', '=', $subscriberdata['id'])->get();
      return ($results);
    }

    public function getLimit($lineguide) {
      // Subscriber Limits (Move to Table)
      // Subscriber = Limit, Marine Limit
      // Inter-Hanover = 2000000, 500000
      // Intact = 1500000, 500000

      if ($lineguide['subscriber'] != null) {
        $entry =  \App\Subscribers::where('identifier', '=', $lineguide['subscriber'])->first();
        $limit = $entry->limit;
      } else {
        $limit = 0;
      }

      // TODO Check for Provincial or IAO Code Exceptions Here
        $iaocodes[] = $lineguide['iao'];
        $exceptions = Self::getExclusions($lineguide['subscriber']);

        $codelist = $lineguide['other'];

        if (strpos($codelist, ',') !== false) {
          $iaocodes = explode(",", $codelist);
          $iaocodes[] = $lineguide['iao'];
          $iaocodes = array_unique($iaocodes);
        } else {
          $iaocodes[] = $lineguide['other'];
        }
        foreach ($exceptions as $exception) {

          if ($exception['hardlimit']) {

            if ($exception['province'] != null) {
              if (($lineguide['province'] == $exception['province']) && ($exception['iao'] == '0')) {
                $limit = $exception['modified_limit'];
              }
            }

            if (in_array($exception->iao, $iaocodes)) {
              if ($exception['province'] != null) {
                  $limit = $exception['modified_limit'];
              } else {
                $limit = 0;
              }
            }

          }
        }

        if (strtolower($lineguide['riskgrade']) == 'good') {

          $limit = $limit * 1.0;

        } elseif (strtolower($lineguide['riskgrade']) == 'average') {

          $limit = Self::round_up_to_nearest_n($limit * 0.75, 50000);

        } elseif (strtolower($lineguide['riskgrade']) == 'belowaverage') {

          $limit = Self::round_up_to_nearest_n($limit * 0.5, 50000);

        } else {
          $limit = 0;
        }

        if (($lineguide['uwlevel'] == '1') || ($lineguide['uwlevel'] == '0')) {

          $limit = $limit * 1.0;

        } elseif ($lineguide['uwlevel'] == '2') {

          $limit = Self::round_up_to_nearest_n($limit * 0.75, 50000);

        } elseif ($lineguide['uwlevel'] == '3') {

          $limit = Self::round_up_to_nearest_n($limit * 0.75 * 0.75, 50000);

        } else {
          $limit = 0;
        }

        if (($lineguide['towngrade'] >= '1') && ($lineguide['towngrade'] <= '4')) {

          $limit = $limit * 1.0;

        } elseif (($lineguide['towngrade'] >= '5') && ($lineguide['towngrade'] <= '7')) {

          $limit = Self::round_up_to_nearest_n($limit * 0.75, 50000);

        } elseif (($lineguide['towngrade'] >= '8') && ($lineguide['towngrade'] <= '10')) {

          $limit = Self::round_up_to_nearest_n($limit * 0.75 * 0.65, 50000);

        } else {
          $limit = 0;
        }

        if ($lineguide['buildinggrade'] == '1') {

          $limit = $limit * 1.0;

        } elseif ($lineguide['buildinggrade'] == '2')  {

          $limit = Self::round_up_to_nearest_n($limit * 0.75, 50000);

        } elseif ($lineguide['buildinggrade'] == '3')  {

          $limit = Self::round_up_to_nearest_n($limit * 0.75 * 0.65, 50000);

        } elseif (($lineguide['buildinggrade'] == '4') || ($lineguide['buildinggrade'] == '5'))  {

          $limit = Self::round_up_to_nearest_n($limit * 0.75 * 0.65, 50000);

        } else {
          $limit = 0;
        }

        $result = (int)$limit;

        return $result;
    }

    public function round_up_to_nearest_n($value, $n) {
      return ceil($value / $n) * $n;
    }

    public function getSubscriberList()
    {
      $subscribers = Subscribers::orderBy("priority")->get();

      return $subscribers;
    }


    public function getTotalCapacity(Request $request)
    {
      //echo '<pre>',print_r($request),'</pre>';
      //dd($request);

      $uwlevel = $request['uwlevel'];
      $towngrade = ltrim($request['towngrade'], '0');
      $buildinggrade = ltrim($request['buildinggrade'], '0');
      $riskgrade = $request['riskgrade'];
      $province = $request['province'];
      $iao = $request['iao'];
      $other = $request['other'];

      $subscribers = Subscribers::orderBy('priority')->get();

      $totallimit = 0;

      foreach($subscribers as $subscriber) {
          $guide = Self::getLineGuide($subscriber['identifier'], $riskgrade, $uwlevel, $towngrade, $buildinggrade, $province, $iao, $other);
          $limits[] = array (
            'subscriber' => $subscriber['name'],
            'limit' => $guide,
          );
          $totallimit += $guide;
      }
      return $totallimit;

    }

    public function getSubscribers(Request $request)
    {
      //echo '<pre>',print_r($request),'</pre>';
      //dd($request);

      $uwlevel = $request['uwlevel'];
      $towngrade = ltrim($request['towngrade'], '0');
      $buildinggrade = ltrim($request['buildinggrade'], '0');
      $riskgrade = $request['riskgrade'];
      $province = $request['province'];
      $iao = $request['iao'];
      $other = $request['other'];
      $subscribers = Subscribers::orderBy('priority')->get();

      foreach($subscribers as $subscriber) {
          $guide = Self::getLineGuide($subscriber['identifier'], $riskgrade, $uwlevel, $towngrade, $buildinggrade, $province, $iao, $other);


          $limits[] = array (
            'id' => $subscriber['id'],
            'subscriber' => $subscriber['name'],
            'identifier' => $subscriber['identifier'],
            'limit' => $guide,
          );
        }
      return $limits;
    }

    public function getMessages(Request $request)
    {
      $iaocodes[] = $request['iao'];
      $messages = array();
      $iao = $request['iao'];
      $codelist = $request['other'];

      // if ($other == null) {
      //   $other = 'not provided';
      // }


      if (strpos($codelist, ',') !== false) {
        $iaocodes = explode(",", $codelist);
        $iaocodes[] = $iao;
        $iaocodes = array_unique($iaocodes);
      } else {
        $iaocodes[] = $request['other'];
      }


      $subscribers = Subscribers::orderBy('priority')->get();

      foreach ($subscribers as $subscriber) {
        $results = Exclusions::where('subscriber_id', '=', $subscriber['id'])->get();
        foreach($results as $result) {

          foreach($iaocodes as $iaocode) {

            if ($result['iao'] == $iaocode) {
              $messages[] = array($result['message'], $subscriber['id'], $iaocode, $result['referral']);
            }
          }
        }
      }
      return $messages;

    }
}
