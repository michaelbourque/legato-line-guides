<?php

namespace App\Http\Controllers;

use App\Subscribers;
use App\Pdf;

class PDFController extends Controller
{
    /**
     * This generates a PDF document based on the parameters provided from defaultController.
     *
     * @param object $data composite object formulated by defaultController
     *
     * @return object PDF document
     */
    public function generatePDF($data)
    {
        $pdf = new Pdf($data['submission']);                // Name the PDF
        $pdf->SetDisplayMode('fullwidth', 'continuous');    // Dimension it
        $pdf->setTitle('Line Guides');                      // Give it a TITLE

        $pdf->Header(); // Insert the header
        $pdf->Footer(); // Insert the footer

        $pdf->SetFont('helvetica', 'B', 20);
        $pdf->setCellPadding(1.7);
        $pdf->setY($pdf->getY() + 10);

        $pdf->SetFont('helvetica', 'B', 10);
        $pdf->MultiCell(65, 0, 'Submission:', 0, 'L', 0, 0);
        $pdf->MultiCell(5, 0, '', 0, 'C', 0, 0);
        $pdf->MultiCell(65, 0, 'Town Grade:', 0, 'L', 0, 0);
        $pdf->MultiCell(0, 0, '', 0, 'C', 0, 1);

        $pdf->SetFont('helvetica', '', 10);
        $pdf->MultiCell(65, 0, $data['submission'], 1, 'L', 0, 0);
        $pdf->MultiCell(5, 0, '', 0, 'C', 0, 0);
        $pdf->MultiCell(65, 0, $data['towngrade'], 1, 'L', 0, 0);
        $pdf->SetFont('helvetica', 'B', 10);
        $pdf->MultiCell(5, 0, '', 0, 'C', 0, 0);
        $pdf->MultiCell(0, 0, 'Total Capacity', 0, 'C', 0, 1);

        $pdf->MultiCell(65, 0, 'TIV:', 0, 'L', 0, 0);
        $pdf->MultiCell(5, 0, '', 0, 'C', 0, 0);
        $pdf->MultiCell(65, 0, 'Construction:', 0, 'L', 0, 0);
        $pdf->MultiCell(5, 0, '', 0, 'C', 0, 0);
        $pdf->MultiCell(0, 0, $data['totalcapacity_field'], 0, 'C', 0, 1);

        $pdf->SetFont('helvetica', '', 10);
        $pdf->MultiCell(65, 0, $data['tiv'], 1, 'L', 0, 0);
        $pdf->MultiCell(5, 0, '', 0, 'C', 0, 0);
        $pdf->MultiCell(65, 0, $data['constructiongrade'], 1, 'L', 0, 0);
        $pdf->MultiCell(5, 0, '', 0, 'C', 0, 0);
        $pdf->MultiCell(0, 0, '', 0, 'C', 0, 1);

        $pdf->SetFont('helvetica', 'B', 10);
        $pdf->MultiCell(65, 0, 'Underwriter:', 0, 'L', 0, 0);
        $pdf->MultiCell(5, 0, '', 0, 'C', 0, 0);
        $pdf->MultiCell(65, 0, 'Underwriter Level:', 0, 'L', 0, 0);
        $pdf->MultiCell(5, 0, '', 0, 'C', 0, 0);
        $pdf->MultiCell(0, 0, '', 'LTR', 'C', 0, 1);

        $pdf->SetFont('helvetica', '', 10);
        $pdf->MultiCell(65, 0, $data['underwriter'], 1, 'L', 0, 0);
        $pdf->MultiCell(5, 0, '', 0, 'C', 0, 0);
        $pdf->MultiCell(65, 0, $data['uwclass'], 1, 'L', 0, 0);
        $pdf->MultiCell(5, 0, '', 0, 'C', 0, 0);
        $pdf->MultiCell(0, 0, '', 'LR', 'C', 0, 1);

        $pdf->SetFont('helvetica', 'B', 10);
        $pdf->MultiCell(65, 0, 'Policy:', 0, 'L', 0, 0);
        $pdf->MultiCell(5, 0, '', 0, 'C', 0, 0);
        $pdf->MultiCell(65, 0, 'Risk Class:', 0, 'L', 0, 0);
        $pdf->MultiCell(5, 0, '', 0, 'C', 0, 0);
        $pdf->SetFont('helvetica', 'I', 10);
        $pdf->MultiCell(0, 0, 'Referral Signature', 'LBR', 'C', 0, 1);

        $pdf->SetFont('helvetica', '', 10);
        $pdf->MultiCell(65, 0, $data['policy'], 1, 'L', 0, 0);
        $pdf->MultiCell(5, 0, '', 0, 'C', 0, 0);
        $pdf->MultiCell(65, 0, $data['riskgrading'], 1, 'L', 0, 0);
        $pdf->MultiCell(5, 0, '', 0, 'C', 0, 0);
        $pdf->MultiCell(0, 10, '', 0, 'C', 0, 1);

        $pdf->Line(10, $pdf->getY(), 200, $pdf->getY());

        $pdf->SetFont('helvetica', 'B', 10);
        $pdf->MultiCell(0, 1, 'Warnings and Errors:', 0, 'L', 0, 0);
        $pdf->SetFont('helvetica', '', 10);
        $pdf->MultiCell(0, 5, '', 0, 'C', 0, 1);
        $pdf->MultiCell(190, 0, strip_tags($data['capacitymessages_field']).', '.strip_tags(str_replace(')', '), ', $data['exclusionmessages_field'])).', '.strip_tags($data['warningmessages_field']), 0, 'L', 0, 0);
        $pdf->MultiCell(0, 0, '', 0, 'C', 0, 1);

        $pdf->SetY($pdf->getY() + 10);
        $pdf->Line(10, $pdf->getY(), 200, $pdf->getY());
        $pdf->SetY($pdf->getY() + 2);

        // Get a list of all subscribers
        $subscribersall = Subscribers::all();
        // Create a bin to hold required subscribers
        $subscribers = [];

        // for each of the subscribers
        foreach ($subscribersall as $subscriber) {
            // get the company identifier
            $company = $subscriber->identifier;
            // if the company is in data (with a percentage)
            if (isset($data[$company.'percent'])) {
                // add it to the subscibers array
                $subscribers[] = $subscriber;
            }
        }

        $pdf->SetFont('helvetica', 'B', 10);
        foreach ($subscribers as $subscriber) {
            $company = $subscriber->identifier;
            $name = $subscriber->name;

            $pdf->MultiCell(85, 0, $name.':', 0, 'R', 0, 0);
            $pdf->MultiCell(20, 0, $data[$company.'percent'], 1, 'R', 0, 0);
            $pdf->MultiCell(5, 0, '', 0, 'C', 0, 0);
            $pdf->MultiCell(25, 0, $data[$company.'value'], 1, 'R', 0, 0);
            $pdf->MultiCell(5, 0, '', 0, 'C', 0, 0);
            $pdf->MultiCell(20, 0, 'Max.Cap.', 0, 'R', 0, 0);
            $pdf->MultiCell(5, 0, '', 0, 'C', 0, 0);
            $pdf->MultiCell(25, 0, $data[$company], 1, 'R', 0, 0);
            $pdf->MultiCell(0, 10, '', 0, 'C', 0, 1);
            // }
        }
    //$pdf->MultiCell(0, 0, "", "T", "C", 0, 1);
    $pdf->Line(10, $pdf->getY(), 200, $pdf->getY());
        $pdf->SetY($pdf->getY() + 2);
        $pdf->MultiCell(90, 0, 'Totals:', 0, 'R', 0, 0);
        $pdf->MultiCell(15, 0, $data['totalpercent_field'], 1, 'R', 0, 0);
        $pdf->MultiCell(5, 0, '', 0, 'C', 0, 0);
        $pdf->MultiCell(25, 0, $data['totalvalue_field'], 1, 'R', 0, 0);
        $pdf->MultiCell(5, 0, '', 0, 'C', 0, 0);
        $pdf->MultiCell(20, 0, 'Max.Cap.', 0, 'R', 0, 0);
        $pdf->MultiCell(5, 0, '', 0, 'C', 0, 0);
        $pdf->MultiCell(25, 0, $data['totalcapacity_field'], 1, 'R', 0, 0);
        $pdf->MultiCell(0, 10, '', 0, 'C', 0, 1);

        $pdf->SetFont('helvetica', 'B', 10);

        $pdf->Line(10, $pdf->getY(), 200, $pdf->getY());

        if ($data['notes'] != '') {
            $pdf->SetFont('helvetica', 'B', 10);
            $pdf->MultiCell(0, 0, 'Notes:', 0, 'L', 0, 1);
            $pdf->SetFont('helvetica', '', 10);
            //$height = Self::checkHeights($pdf, array(array("width" => 0, "text" => $data["notes"])));
            $pdf->MultiCell(0, $pdf->getY() + 5, $data['notes'], 0, 'L', 0, 1);

            if (count($data['cell']) == 1 && $data['cell'][1][1] == '') {
            } else {
                for ($i = 1; $i <= count($data['cell']); ++$i) {
                    $width = 190 / count($data['cell'][$i]);
                    for ($j = 1; $j <= count($data['cell'][$i]); ++$j) {
                        if ($j != count($data['cell'][$i])) {
                            $pdf->MultiCell($width, 0, $data['cell'][$i][$j], 1, 'L', 0, 0);
                        } else {
                            $pdf->MultiCell($width, 0, $data['cell'][$i][$j], 1, 'L', 0, 1);
                        }
                    }
                }
            }
        }

        return $pdf->output('/'.$data['filename'], 'S');
    }

    public function checkHeights($pdf, $array, $noBorder = null)
    {
        $max_height = 0;
        if ($noBorder) {
            foreach ($array as $item) {
                $current_height = $pdf->getStringHeight($item['width'], $item['text'], false, true, '', 0);
                if ($current_height > $max_height) {
                    $max_height = $current_height;
                }
            }
        } else {
            foreach ($array as $item) {
                $current_height = $pdf->getStringHeight($item['width'], $item['text'], false, true, '', 1);
                if ($current_height >= $max_height) {
                    $max_height = $current_height;
                }
            }
        }

        $page_height = $pdf->getPageHeight();
        $margins = $pdf->getMargins();

        if ($pdf->GetY() + $max_height >= ($page_height - $margins['bottom'] - 10)) {
            $pdf->setHeaderIndex($pdf->getHeaderIndex() - 1);
            $pdf->addPage();
            $pdf->setFooterIndex($pdf->getFooterIndex() - 1);
        }

        return $max_height;
    }
}
