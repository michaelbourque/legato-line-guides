<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SugarBowl\SB_Submission;
use File;

class defaultController extends Controller
{
    /**
     * Returns an error/404 view if the url is not properly used.
     *
     * @return view errors.404
     */
    public function getIndex()
    {
        return view('errors.404');
    }

    /**
     * Recveives data from #lgform, creates, and saves a PDF LineGuide to the Submission folder.
     *
     * @param Request $request Laravel object containing form data from #lg_form
     *
     * @return view saved
     */
    public function postIndex(Request $request)
    {
        // Simplify the request structure
        $inputs = $request->input();

         // If no submission is provided, Die since we cannot save the file anyway
        if ($inputs['submission'] == '') {
            die('Missing Submission');
        // otherwise get the Submission data from SuiteCRM using SugarBowl
        } else {
            $submission = SB_Submission::where('name', '=', $inputs['submission'])->where('deleted', '=', 0)->first();
        }
        // Generate a PDF using PDFCController
        $pdf = \App::make("App\Http\Controllers\PDFController")->generatePDF($inputs);

        // Get the application environment
        $env = app()->environment();

        // If the app environment is local
        if ($env == 'local') {
            // set the file path to the SUBMISSION_DIRECTORY
            $path = env('SUBMISSION_DIRECTORY');
            // if the path does not exist
            if (!file_exists($path)) {
                // create the target folder with 777 permissions
                mkdir($path, 0777, true);
            }
            // save the File
            \File::put($path.$inputs['filename'], $pdf);
        // otherwise (if not local)
        } else {
            // Save in Submission Directory
            $term = date('Y').'-'.date('y', strtotime('+1 year'));
            $path = env('SUBMISSION_DIRECTORY').'/'.date('Y', strtotime($submission->date_entered)).'/'.$inputs['submission'].'/'.$term.'/Underwriting Correspondence/';
            if (!file_exists($path)) {
                mkdir($path, 0777, true);
                chown($path, 1002);
                chgrp($path, 1002);
                chmod($path, 0777);
            } else {
                chown($path, 1002);
                chgrp($path, 1002);
                chmod($path, 0777);
            }

            // Create other missing folders since we're here
            $path2 = env('SUBMISSION_DIRECTORY').'/'.date('Y', strtotime($submission->date_entered)).'/'.$inputs['submission'].'/'.$term.'/Inspections/';
            $path3 = env('SUBMISSION_DIRECTORY').'/'.date('Y', strtotime($submission->date_entered)).'/'.$inputs['submission'].'/'.$term.'/Applications-Quotes/';
            $path4 = env('SUBMISSION_DIRECTORY').'/'.date('Y', strtotime($submission->date_entered)).'/'.$inputs['submission'].'/'.$term.'/Claims/';
            $path5 = env('SUBMISSION_DIRECTORY').'/'.date('Y', strtotime($submission->date_entered)).'/'.$inputs['submission'].'/'.$term.'/Floaters/';
            if (!file_exists($path2)) {
                mkdir($path2, 0777, true);
                chown($path2, 1002);
                chgrp($path2, 1002);
                chmod($path2, 0777);
            }
            if (!file_exists($path3)) {
                mkdir($path3, 0777, true);
                chown($path3, 1002);
                chgrp($path3, 1002);
                chmod($path3, 0777);
            }
            if (!file_exists($path4)) {
                mkdir($path4, 0777, true);
                chown($path4, 1002);
                chgrp($path4, 1002);
                chmod($path4, 0777);
            }
            if (!file_exists($path5)) {
                mkdir($path5, 0777, true);
                chown($path5, 1002);
                chgrp($path5, 1002);
                chmod($path5, 0777);
            }
            // save the file and update the permissions
            File::put($path.'/'.$inputs['filename'], $pdf);
            chown($path.'/'.$inputs['filename'], 1002);
            chgrp($path.'/'.$inputs['filename'], 1002);
            chmod($path.'/'.$inputs['filename'], 0777);
        }

        // create path to document directory
        $dd_link = 'file://Z:/Submissions/'.date('Y', strtotime($submission->submission_date)).'/'.$submission->name.'/'.date('Y').'-'.date('y', strtotime(date('Y', strtotime($submission->submission_date)).'+ 1 year')).'/Underwriting Correspondence';

        // create the data array
        $data = array(
            'message' => '<b>'.$inputs['filename'].'</b> saved to <a id="dd_link" href="'.$dd_link.'" class="alert-link" data-toggle="tooptip" title="Open in Ext. App.">Documents Directory.</a>',
            'type' => 'success',
            'class' => 'glyphicon glyphicon-ok',
        );

        // return the 'saved' view with data array
            return view('saved')->with('data', $data);
    }
}
