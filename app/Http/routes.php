<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'limitController@getEmptyForm');
Route::post('/submit', 'defaultController@postIndex');
Route::get('/saved', 'defaultController@getSaved');
Route::get('/nosubmission', ['as' => 'nosubmission', function() {
    return View::make("sub_not_found");
}]);

//Route::get('/lineguide/{subscriber}/{riskgrade}/{uwlevel}/{towngrade}/{buildinggrade}/{province}/{iao}', 'limitController@getLineGuide');
Route::get('/{submission}', 'limitController@getLimitsBySubmission');
Route::post('/totalcapacity', 'limitController@getTotalCapacity');
Route::post('/getsubscribers', 'limitController@getSubscribers');
Route::post('/getmessages', 'limitController@getMessages');
