<?php

namespace App\SugarBowl;

use Illuminate\Database\Eloquent\Model;

class SB_Task extends Model 
{
	protected $connection = 'sugar';
	protected $table = 'tasks';

	public function user($closure = false) {
		if(!$closure){
			return $this->belongsTo("App\SugarBowl\SB_User", "assigned_user_id");
		} 
		return $this->belongsTo("App\SugarBowl\SB_User", "assigned_user_id")->first();
	}

	public function submission($closure = false) {
		if(!$closure){
			return $this->belongsToMany("App\SugarBowl\SB_Submission", "w887_submissions_tasks", "tasks_idb", "w887_submissions_ida")->where("w887_submissions_tasks.deleted", "=", 0);
		} 
		return $this->belongsToMany("App\SugarBowl\SB_Submission", "w887_submissions_tasks", "tasks_idb", "w887_submissions_ida")->where("w887_submissions_tasks.deleted", "=", 0)->first();
	}
}