<?php

namespace App\SugarBowl;

use Illuminate\Database\Eloquent\Model;
use App\SugarBowl\SB_user;

class SB_Policy extends Model 
{
	protected $connection = 'sugar';
	protected $table = 'w887_policies';

	public function submissions($closure = false){
		if(!$closure){
			return $this->belongsToMany("App\SugarBowl\SB_Submission", "w887_polici_submissions", "w887_policies_ida", "w887_submissions_idb")->where("w887_polici_submissions.deleted", "=", 0);
		} 
		return $this->belongsToMany("App\SugarBowl\SB_Submission", "w887_polici_submissions", "w887_policies_ida", "w887_submissions_idb")->where("w887_polici_submissions.deleted", "=", 0)->get();
	}
}