<?php

namespace App\SugarBowl;

use Illuminate\Database\Eloquent\Model;
use App\SugarBowl\SB_user;

class SB_Location extends Model 
{
	protected $connection = 'sugar';
	protected $table = 'w887_property_rating';

	public function submissions($closure = false) {
		if(!$closure){
			return $this->belongsToMany("App\SugarBowl\SB_Submission", "w887_proper_submissions", "w887_property_rating_ida", "w887_submissions_idb")->where("w887_proper_submissions.deleted", "=", 0);
		} 
		return $this->belongsToMany("App\SugarBowl\SB_Submission", "w887_proper_submissions", "w887_property_rating_ida", "w887_submissions_idb")->where("w887_proper_submissions.deleted", "=", 0)->get();
	}
}