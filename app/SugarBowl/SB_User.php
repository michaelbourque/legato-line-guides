<?php

namespace App\SugarBowl;

use Illuminate\Database\Eloquent\Model;

class SB_User extends Model 
{
    protected $connection = 'sugar';
    protected $table = 'users';

    public function submissions($closure = false) {
        if($closure){
            return $this->hasMany("App\SugarBowl\SB_Submission", "assigned_user_id")->where("w887_submissions.deleted", "=", 0);
        } 
        return $this->hasMany("App\SugarBowl\SB_Submission", "assigned_user_id")->where("w887_submissions.deleted", "=", 0)->get();
    }

    public function email($closure = false) {
        if($closure){
            return $this->belongsToMany("App\SugarBowl\SB_Email", "email_addr_bean_rel", "bean_id", "email_address_id")->where("bean_module", "=", "Users")->where("email_addr_bean_rel.deleted", "=", 0);
        } 
        return $this->belongsToMany("App\SugarBowl\SB_Email", "email_addr_bean_rel", "bean_id", "email_address_id")->where("bean_module", "=", "Users")->where("email_addr_bean_rel.deleted", "=", 0)->first();
    }

    public function tasks($status = null, $closure = false) {
    	if($status != null){
            if($closure){
                return $this->hasMany("App\SugarBowl\SB_Task", "assigned_user_id")->where("status", "=", $status);
            } 
            return $this->hasMany("App\SugarBowl\SB_Task", "assigned_user_id")->where("status", "=", $status)->get();
        } else {
            if($closure){
                return $this->hasMany("App\SugarBowl\SB_Task", "assigned_user_id");
            } 
            return $this->hasMany("App\SugarBowl\SB_Task", "assigned_user_id")->get();
        }
    }
}