<?php

namespace App\SugarBowl;

use Illuminate\Database\Eloquent\Model;

class SB_Contact extends Model 
{
	protected $connection = 'sugar';
	protected $table = 'contacts';

	public function company($closure = false) {
		if(!$closure){
			return $this->belongsToMany("App\SugarBowl\SB_Company", "accounts_contacts", "contact_id", "account_id")->where("accounts_contacts.deleted", "=", 0);
		} 
		return $this->belongsToMany("App\SugarBowl\SB_Company", "accounts_contacts", "contact_id", "account_id")->where("accounts_contacts.deleted", "=", 0)->first();
	}

	public function email($closure = false) {
		if(!$closure){
			return $this->belongsToMany("App\SugarBowl\SB_Email", "email_addr_bean_rel", "bean_id", "email_address_id")->where("bean_module", "=", "Contacts")->where("email_addr_bean_rel.deleted", "=", 0);
		} 
		return $this->belongsToMany("App\SugarBowl\SB_Email", "email_addr_bean_rel", "bean_id", "email_address_id")->where("bean_module", "=", "Contacts")->where("email_addr_bean_rel.deleted", "=", 0)->first();
	}
}