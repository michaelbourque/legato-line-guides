<?php

namespace App\SugarBowl;

use Illuminate\Database\Eloquent\Model;
use App\SugarBowl\SB_user;

class SB_Submission extends Model 
{
    protected $connection = 'sugar';
    protected $table = 'w887_submissions';

    public function user() {
        if(!$closure){
            return $this->belongsTo("App\SugarBowl\SB_User", "assigned_user_id");
        } 
        return $this->belongsTo("App\SugarBowl\SB_User", "assigned_user_id")->first();
    }

    public function tasks($status = null, $closure = false) {
        if($status != null){
            if(!$closure){
                return $this->belongsToMany("App\SugarBowl\SB_Task", "w887_submissions_tasks", "w887_submissions_ida", "tasks_idb")->where("w887_submissions_tasks.deleted", "=", 0)->where("status", "=", $status);
            } 
            return $this->belongsToMany("App\SugarBowl\SB_Task", "w887_submissions_tasks", "w887_submissions_ida", "tasks_idb")->where("w887_submissions_tasks.deleted", "=", 0)->where("status", "=", $status)->get();
        } else {
            if(!$closure){
                return $this->belongsToMany("App\SugarBowl\SB_Task", "w887_submissions_tasks", "w887_submissions_ida", "tasks_idb")->where("w887_submissions_tasks.deleted", "=", 0);
            } 
            return $this->belongsToMany("App\SugarBowl\SB_Task", "w887_submissions_tasks", "w887_submissions_ida", "tasks_idb")->where("w887_submissions_tasks.deleted", "=", 0)->get();
        }
    }

    public function policies($closure = false) {
        if(!$closure){
            return $this->belongsToMany("App\SugarBowl\SB_Policy", "w887_polici_submissions", "w887_submissions_idb", "w887_policies_ida")->where("w887_polici_submissions.deleted", "=", 0);
        } 
        return $this->belongsToMany("App\SugarBowl\SB_Policy", "w887_polici_submissions", "w887_submissions_idb", "w887_policies_ida")->where("w887_polici_submissions.deleted", "=", 0)->get();
    }

    public function locations($closure = false) {
        if(!$closure){
            return $this->belongsToMany("App\SugarBowl\SB_Location", "w887_proper_submissions", "w887_submissions_idb", "w887_property_rating_ida")->where("w887_proper_submissions.deleted", "=", 0);
        } 
        return $this->belongsToMany("App\SugarBowl\SB_Location", "w887_proper_submissions", "w887_submissions_idb", "w887_property_rating_ida")->where("w887_proper_submissions.deleted", "=", 0)->get();
    }

    public function insured($closure = false) {
        if(!$closure){
            return $this->belongsToMany("App\SugarBowl\SB_Insured", "w887_insure_submissions", "w887_submissions_idb", "w887_insured_ida")->where("w887_insure_submissions.deleted", "=", 0);
        } 
        return $this->belongsToMany("App\SugarBowl\SB_Insured", "w887_insure_submissions", "w887_submissions_idb", "w887_insured_ida")->where("w887_insure_submissions.deleted", "=", 0)->get();
    }

    public function company($closure = false) {
        if(!$closure){
            return $this->belongsToMany("App\SugarBowl\SB_Company", "w887_submisons_accounts", "w887_submissions_ida", "accounts_idb")->where("w887_submisons_accounts.deleted", "=", 0);
        } 
        return $this->belongsToMany("App\SugarBowl\SB_Company", "w887_submisons_accounts", "w887_submissions_ida", "accounts_idb")->where("w887_submisons_accounts.deleted", "=", 0)->get();
    }
}