<?php

namespace App\SugarBowl;

use Illuminate\Database\Eloquent\Model;

class SB_Email extends Model 
{
    protected $connection = 'sugar';
    protected $table = 'email_addresses';
}