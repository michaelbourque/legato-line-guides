<?php

namespace App\SugarBowl;

use Illuminate\Database\Eloquent\Model;

class SB_Company extends Model 
{
	protected $connection = 'sugar';
	protected $table = 'accounts';

	public function submissions($closure = false) {
		if(!$closure){
			return $this->belongsToMany("App\SugarBowl\SB_Submission", "w887_submisons_accounts", "accounts_idb", "w887_submissions_ida")->where("w887_submisons_accounts.deleted", "=", 0);
		} 
		return $this->belongsToMany("App\SugarBowl\SB_Submission", "w887_submisons_accounts", "accounts_idb", "w887_submissions_ida")->where("w887_submisons_accounts.deleted", "=", 0)->get();

	}

	public function contacts($closure = false) {
		if(!$closure){
			return $this->belongsToMany("App\SugarBowl\SB_Contact", "accounts_contacts", "account_id", "contact_id")->where("accounts_contacts.deleted", "=", 0);
		} 
		return $this->belongsToMany("App\SugarBowl\SB_Contact", "accounts_contacts", "account_id", "contact_id")->where("accounts_contacts.deleted", "=", 0)->get();
	}
}