<?php

namespace App\SugarBowl;

use Illuminate\Database\Eloquent\Model;

class SB_Insured extends Model 
{
	protected $connection = 'sugar';
	protected $table = 'w887_insured';

	public function submissions($closure = false) {
		if(!$closure){
			return $this->belongsToMany("App\SugarBowl\SB_Submission", "w887_insure_submissions", "w887_insured_ida", "w887_submissions_idb")->where("w887_insure_submissions.deleted", "=", 0);
		} 
		return $this->belongsToMany("App\SugarBowl\SB_Submission", "w887_insure_submissions", "w887_insured_ida", "w887_submissions_idb")->where("w887_insure_submissions.deleted", "=", 0)->get();
	}
}