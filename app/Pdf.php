<?php

namespace App;

class Pdf extends \Elibyy\TCPDF\TCPdf {

    public $header_index;
    public $footer_index;
    public $submission;

    public function __construct($submission){
        $this->submission = $submission;
        parent::__construct("App");
    }

    public function Header() {
        $this->SetFont('helvetica', 'B', 12);
        $this->setY(2);
        $this->Write(0, 'Line Guides', '', 0, 'C', 0, 0, false, false, 0);
        $this->header_index++;
    }

    // Page footer
    public function Footer() {
        $this->SetFont('helvetica', 'B', 8);
        $this->setY(-10);
        $this->Cell(45, 0, "Submission ".$this->submission, 0, 0, 'L', 0, '', 0, false, 'T', 'M');

        $this->Cell(63, 0, "Line Guides v3.0", 0, 0, 'C', 0, '', 0, false, 'T', 'M');
        $this->Cell(63, 0, date("M/d/Y"), 0, 0, 'R', 0, '', 0, false, 'T', 'M');
        $this->footer_index++;
    }

    public function setHeaderIndex($index){
        $this->header_index = $index;
    }

    public function setFooterIndex($index){
        $this->footer_index = $index;
    }
    public function getHeaderIndex(){
        return $this->header_index;
    }
    public function getFooterIndex(){
        return $this->footer_index;
    }
}
