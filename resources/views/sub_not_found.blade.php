<!DOCTYPE html>
<html>
	<head>
    <meta charset="UTF-8" />    
		<title>LineGuides 3.0 - Not Found</title>
		<link href='//fonts.googleapis.com/css?family=Lato:100' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

		<style>
			body {
				margin: 0;
				padding: 0;
				width: 100%;
				height: 100%;
				color: #888;
				display: table;
				font-weight: 100;
				font-family: 'Lato';
			}

			.container {
				text-align: center;
				display: table-cell;
				vertical-align: middle;
			}

			.content {
				text-align: center;
				display: inline-block;
			}

			.title {
				font-size: 72px;
				margin-bottom: 40px;
			}

      .custom-primary {
        color: #428bca;
      }

		</style>
	</head>
	<body>
		<div class="container">
			<div class="content">

        <h1>LineGuides 3.0</h1>
				<div class="title"><i class="fa fa-warning fa-3x custom-primary"></i></div>
				<h2>This submission number cannot be found.</h2>
        <h3>Please correct the number or</h3>
				<h4>contact <a href="mailto:helpdesk@amfredericks.com?Subject=LineGuides%203.0%20-%20Help%20request" target="_top">HelpDesk</a> if you need assistance.</h4>

			</div>
		</div>
	</body>
</html>
