<!DOCTYPE html>
<html>
    <head>
        <title>LEGATO - LineGuides {!! env('VERSION') !!}</title>

        <link rel="stylesheet" href="{{ asset("assets/css/font-awesome.min.css") }}"/>
        <link rel="stylesheet" href="{{ asset("assets/css/bootstrap.min.css") }}"/>
        <link rel="stylesheet" href="{{ asset("assets/css/bootstrap-theme.min.css") }}"/>
        <!--<link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">-->
        <!--<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css"> -->
        <link rel="stylesheet" href="{{ asset("assets/css/legato-custom.css") }}" type="text/css" media="screen"/>
        <link rel="stylesheet" href="{{ asset("assets/css/legato-print.css") }}" type="text/css" media="print"/>
        <link rel="stylesheet" href="{{ asset("assets/css/bootstrap-select.min.css") }}" type="text/css" media="screen"/>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
        <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
        <!--<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>-->
        <script src="{{ asset('assets/js/autoNumeric.js') }}"></script>
        <script src="{{ asset('assets/js/legato-definitions.js') }}"></script>
        <script src="{{ asset('assets/js/legato-custom.js') }}"></script>
        <script src="{{ asset('assets/js/legato-interface.js') }}"></script>
        <script src="{{ asset('assets/js/legato-notesgrid.js') }}"></script>
        <script src="{{ asset('assets/js/bootstrap-select.min.js') }}"></script>

    </head>
    <body>
      @if( isset($data['error']) )
          <div class="alert alert-{{ $data['error']['type'] }} alert-dismissible" data-fade="false" role="alert">
              <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
              <span class=" {{ $data['error']['class'] }} "></span> {!! $data['error']['message'] !!}
          </div>
      @else

  <nav class="navbar navbar-darkgrey navbar-static-top">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">LEGATO - LineGuides {!! env('VERSION') !!}</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

      <ul class="nav navbar-nav navbar-right">
        <li class="disabled"><a href="#" id="print" class="disabled"><i class="fa fa-print"></i> Print</a></li>
        <li class="disabled"><a href="#" id="save" class="disabled"><i class="fa fa-save"></i> Save</a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
    <form id="lg_form" role="form" method="post" action="/lg3/submit">
    <input type="hidden" class="form-control" id="nosub" name="nosub" value="nosub" disabled>
      <div class="container-fluid">

            <div class="row">

              <div class="col-xs-6 col-lg-6 col-sm-6 col-md-6">
                <div class="panel panel-darkgrey">
                  <div class="panel-heading">
                      <h4 class="panel-title">
                          <span class="toggle-view pointer"></span>
                          Property Rating
                      </h4>
                  </div>
                  <div class="panel-body">

                      <input id="fn_full" type="hidden" name="filename" />
                      <input id="totalcapacity_field" type="hidden" name="totalcapacity_field" class="an_currency" />
                      <input id="totalvalue_field" type="hidden" name="totalvalue_field" class="an_currency"/>
                      <input id="totalpercent_field" type="hidden" name="totalpercent_field" class="an_percenttotal" />
                      <input id="capacitymessages" type="hidden" name="capacitymessages_field"/>
                      <input id="exclusionmessages" type="hidden" name="exclusionmessages_field"/>
                      <input id="warningmessages" type="hidden" name="warningmessages_field"/>
                      <div class="form-group">
                        <div class="row">
                          <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <label for="submission">Submission:</label>
                            <input type="text" class="form-control" id="submission" name="submission">
                          </div>
                          <div class="co. '<br />';
                            $errormessage .= 'l-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <label for="policy">Policy:</label>
                            <input type="text" class="form-control" id="policy" name="policy">
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <label for="location">Location:</label>
                            <input type="text" class="form-control" id="location" name="location">
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <label for="underwriter">Underwriter:</label>
                            <select class="form-control" id="underwriter" name="underwriter">
                              <option disabled selected> -- choose -- </option>
                              @foreach ($data as $name)
                                <option value="{{ $name[0] }}" data-uwclass="{{ $name[1] }}">{{ $name[0] }}</option>
                              @endforeach
                            </select>
                          </div>
                          <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <!-- <label for="uwclass">Underwriter Class:</label> -->
                            <input type="hidden" class="form-control" id="uwclass" name="uwclass" value="{{ $data[0][1] }}" disabled>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <label for="towngrade">Town Grade:</label>
                            <input type="number" class="form-control" id="towngrade" name="towngrade" min="1" max="10" value="1">
                          </div>
                          <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <label for="constructiongrade">Construction Type:</label>
                            <select class="form-control" id="constructiongrade" name="constructiongrade">
                              <option disabled selected> -- choose -- </option>
                              <option value="01">Fire Resistive 1</option>
                              <option value="02">Masony Non-Combustible 2</option>
                              <option value="03">Masonry 3</option>
                              <option value="04">Brick Veneer / Wood Frame 4</option>
                            </select>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                            <label for="riskgrading">Risk Grading:</label>
                            <select class="form-control" id="riskgrading" name="riskgrading">
                              <option disabled selected> -- choose -- </option>
                              <option value="good">Good</option>
                              <option value="average">Average</option>
                              <option value="belowaverage">Below Average</option>
                            </select>
                          </div>
                          <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                            <label for="province">Province:</label>
                            <select class="form-control" id="province" name="province">
                              <option disabled selected> -- choose -- </option>
                              <option value="AB">Alberta</option>
                              <option value="BC">British Columbia</option>
                              <option value="MB">Manitoba</option>
                              <option value="NB">New Brunswick</option>
                              <option value="NL">Newfoundland and Labrador</option>
                              <option value="NS">Nova Scotia</option>
                              <option value="NT">Northwest Territories</option>
                              <option value="NU">Nunavut</option>
                              <option value="ON">Ontario</option>
                              <option value="PE">Prince Edward Island</option>
                              <option value="QC">Quebec</option>
                              <option value="SK">Saskatchewan</option>
                              <option value="YT">Yukon</option>
                            </select>
                          </div>
                          <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                            <label for="tiv">TIV:</label>
                            <input type="text" class="form-control an_currency" id="tiv" name="tiv" >
                          </div>

                        </div>
                      </div>
                  </div>
                </div>
                <div class="panel panel-darkgrey">
                  <div class="panel-heading">
                      <h4 class="panel-title">
                          <span class="toggle-view pointer"></span>
                          Industry Codes
                      </h4>
                  </div>
                  <div class="panel-body">
                    <div class="row">
                      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                        <label for="iao">Main IAO:</label>
                        <input type="text" class="form-control" id="iao" name="iao" value="">
                      </div>
                      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                        <label for="otheriao">Other IAO:</label>
                        <input type="text" class="form-control" id="otheriao" name="otheriao" value="">
                      </div>
                    </div>
                  </div>
                </div>

                <div class="panel panel-darkgrey referralsig">
                  <div class="panel-heading">
                      <h4 class="panel-title">
                          Office Use Only
                      </h4>
                  </div>
                  <div class="panel-body">
                      <div id="referral_message"></div>
                      <div class="well well-lg text-center">
                        <br />
                        <br />
                        <br />
                        <span class="text-muted"><small><i>Referral Signature</i></small></span>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-xs-6 col-lg-6 col-sm-6 col-md-6">
                <div class="panel panel-darkgrey">
                  <div class="panel-heading">
                      <h4 class="panel-title">
                          <span class="toggle-view pointer"></span>
                          Capacity
                      </h4>
                  </div>
                  <div class="panel-body">
                    <div id="total_capacity">Total Capacity
                      <span class="tcv an_currency">0</span>
                      <div id="capacity_message"></div>
                      <div id="exclusion_message"></div>
                    </div>
                  </div>
                </div>
                <div class="panel panel-darkgrey" id="subscriberspanel">
                  <div class="panel-heading">
                      <h4 class="panel-title">
                          <span class="toggle-view pointer"></span>
                          Subscribers
                          <button type="button" id="distribute" class="btn btn-darkblue btn-xs pull-right"><i class="fa fa-sliders"></i>Auto-Distribute</button>
                          <button type="button" id="cleardistribute" class="btn btn-darkgrey btn-xs pull-right"><i class="fa fa-eraser"></i>Clear</button>
                      </h4>
                  </div>
                  <div class="panel-body ">
                      <div id="subscribersform" class="form-group">
                      </div>
                    <div class="row">
                      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 totals">
                            <div id="totalvalue" class="an_currency">0</div>
                      </div>
                      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 totals">
                            <div id="totalpercent" class="an_percenttotal">0</div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

      @endif
      <div id="notespanel" class="panel panel-darkgrey">
      		<div class="panel-heading">
      			<h4 class="panel-title">Notes:</h4>
      		</div>
      		<div class="panel-body">
      			<div class="row">
      				<div class="col-xs-12">
      					<div class="form-group">
      						<textarea id="notes" class="form-control" rows="4" name="notes"></textarea>
      					</div>
      				</div>
      			</div>
      			<table id="excel_table" class="table table-bordered table-condensed">
      				<tr class="tbl_row">
      					<td class="tbl_cell">
      						<input class="form-control" id="cell[1][1]" name="cell[1][1]"/>
      					</td>
      				</tr>
      			</table>
      			<div class="text-center hidden-print">
      				<button type="button" class="btn btn-darkgrey" onclick="addRow()"><span class="glyphicon glyphicon-chevron-down"></span></button>
      				<button type="button" class="btn btn-darkgrey" onclick="removeRow()"><span class="glyphicon glyphicon-chevron-up"></span></button>
      				<button type="button" class="btn btn-darkgrey" onclick="addCol()"><span class="glyphicon glyphicon-chevron-right"> </span></button>
      				<button type="button" class="btn btn-darkgrey" onclick="removeCol()"><span class="glyphicon glyphicon-chevron-left"> </span></button>
      			</div>
      		</div>
      		<div class="panel-footer text-muted">
      			<small><em>Fractional amounts in individual subscriber limits will be ignored.</em></small>
      		</div>
      	</div>
          </div>
        </form>
    </body>

    <div id="save_modal" class="modal fade">
  		<div class="modal-dialog">
  			<div class="modal-content">
  				<div class="modal-header">
  					<h4 class="modal-title">Save to Documents Directory</h4>
  				</div>
  				<div class="modal-body">
  					<!-- <div id="submission_valid" class="alert alert-success"><i class="fa fa-check"></i> Submission</div> -->
  					<div id="percent_valid" class="alert alert-success"><i class="fa fa-check"></i> Percent</div>
  					<div id="tiv_valid" class="alert alert-success"><i class="fa fa-check"></i> TIV</div>
  					<div id="capacity_valid" class="alert alert-success"><i class="fa fa-check"></i> Capacity</div>
  					<label class="control-label">File Name:</label>
  					<div class="input-group">
  						<span id="filenamehint" class="input-group-addon">LineGuide<span id="fn_submission"></span></span>
  						<input id="filenametag" type="text" class="form-control"/>
  						<span class="input-group-addon">.pdf</span>
  					</div>
  					<div class="help-block"><em><small></small></em></div>
  				</div>
  				<div class="modal-footer" style="text-align:left !important;">
  					<span class="text-muted"><em><small>If you wish to print this Line Guide, please print the generated PDF or print <b>before</b> saving.<br/>Application will overwrite any previous Line Guides with the <b>same filename.</b></small></em></span>
  				</div>
  				<div class="modal-footer">
  					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
  					<button id="save_button" type="button" class="btn btn-darkblue">Save</button>
  				</div>
  			</div>
  		</div>
  	</div>
    <script type="text/javascript">
        var data = JSON.parse("{{ json_encode($data) }}".replace(/&quot;/g,'"'));
        $(document).ready(function() {
          init(data);
        });
    </script>
</html>
