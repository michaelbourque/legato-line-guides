<!DOCTYPE html>
<html>
    <head>
        <title>LineGuides 3.0</title>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="{{ asset("assets/css/bootstrap.min.css") }}"/>
        <link rel="stylesheet" href="{{ asset("assets/css/bootstrap-theme.min.css") }}"/>
        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
        <link rel="stylesheet" href="{{ asset("assets/css/slider-custom.css") }}"/>
        <link rel="stylesheet" href="{{ asset("assets/css/legato-custom.css") }}" type="text/css" media="screen"/>
        <link rel="stylesheet" href="{{ asset("assets/css/legato-print.css") }}" type="text/css" media="print">

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
        <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
        <script src="{{ asset('assets/js/autoNumeric.js') }}"></script>
        <script src="{{ asset('assets/js/slider-custom.js') }}"></script>
        <script src="{{ asset('assets/js/legato-custom.js') }}"></script>

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                color: #333333;
                display: table;
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 72px;
                margin-bottom: 40px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="well well-lg">{!! $data['message'] !!}</div>
            </div>
        </div>
    </body>
</html>
