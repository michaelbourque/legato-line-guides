<!DOCTYPE html>
<html>
    <head>
        <title>LineGuides 3.0</title>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="{{ asset("assets/css/bootstrap.min.css") }}"/>
        <link rel="stylesheet" href="{{ asset("assets/css/bootstrap-theme.min.css") }}"/>
        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
        <link rel="stylesheet" href="{{ asset("assets/css/slider-custom.css") }}"/>
        <link rel="stylesheet" href="{{ asset("assets/css/legato-custom.css") }}"/>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
        <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
        <script src="{{ asset('assets/js/autoNumeric.js') }}"></script>
        <script src="{{ asset('assets/js/slider-custom.js') }}"></script>
        <script src="{{ asset('assets/js/legato-custom.js') }}"></script>

    </head>
    <body>

<div class="row">
	<div class="col-xs-12">
		<h2>
		</h2>
		<hr/>
	</div>
</div>
<div class="row">
	<div class="col-xs-12">
		<div class="well well-sm">
			<h4>No New Submissions</h4>
			<p>All new submissions have been properly assigned to an underwriter.</p>
		</div>
	</div>
</div>
</body>
</html>
