<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExclusionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exclusions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('subscriber_id')->unsigned();
            $table->foreign('subscriber_id')->references('id')->on('subscribers');
            $table->string('iao', 4);
            $table->string('referral')->nullable();
            $table->string('province', 2);
            $table->integer('modified_limit');
            $table->boolean('hardlimit')->default(false);
            $table->string('message', 255)->nullable();
            $table->timestamps();
        });

        // Seed with Default Data

        // DB::table('exclusions')->insert(
        //   array(
        //     'subscriber_id' => '1',
        //     'iao' => '',
        //     'province' => '',
        //     'modified_limit' => '',
        //     'hardlimit' => true,
        //     'message' => ''
        //   )
        // );
        //

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('exclusions');
    }
}
