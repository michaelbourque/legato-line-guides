<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscribersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscribers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('identifier');
            $table->integer('limit');
            $table->integer('priority');
            $table->timestamps();
        });

        // Seed with Default Data

        DB::table('subscribers')->insert(
          array(
            'name' => 'International Insurance Company of Hannover SE',
            'identifier' => 'ih',
            'limit' => '2000000',
            'priority' => 1
          )
        );

        DB::table('subscribers')->insert(
          array(
            'name' => 'Intact Insurance Company',
            'identifier' => 'in',
            'limit' => '1500000',
            'priority' => 2
          )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('subscribers');
    }
}
