
[![AM Fredericks](http://dev/stash/projects/APPS/repos/legato---line-guides/browse/public/assets/images/amfLetterheadlogo.png?at=22d92ccfe5e00a76e16f7df5cf278bb1f34c98eb&raw)]()

## Synopsis

Line guides are a useful tool to help Underwriters determine if we have the capacity to write a risk on behalf of participating subscribers. Risk management requires that limits be varied across a wide range of factors that, in combination, make it difficult to manage these limits manually or individually. This project is intended to introduce a linear algorithmic formula that is consistent across all carriers and distributes the risk across all combinations in a consistent manner. We are also attempting to greatly reduce the amount of time and interactive involvement required for an underwriter to produce a line guide solution. Our goal is to full automate the process so that the process is seamless to the quoting process.

## Motivation

* Simplify the user experience
* Automate as much of the process as possible
* Keep maintenance to a minimum (Adding, updating subscribers, changing limits/codes/exceptions)
* Integrate the Line Guide Results with Sugar (By location)
* Integration with Locations in Sugar
* Ability to add IAO exception limits
* Account for Provinces (Exception?)
* Formula based variations of limits
* Referral Engine Integration (doesn't exist yet)


## Requirements

* Laravel 5.1 PHP Web Framework
  * PHP >= 5.6.4
  * OpenSSL PHP Extension
  * PDO PHP Extension
  * Mbstring PHP Extension
  * Tokenizer PHP Extension
* Network Share to Samba: mounted at `/srv/submissions/`

## Installation

- [ ] This project requires PHP 5.6 or greater, so it will need to be installed on APPS2.
- [ ] Clone the latest master branch from [<a href='http://dev/stash/projects/APPS/repos/legato---line-guides/browse'>Here</a>].
- [ ] Use composer to install the Laravel Framework
- [ ] Configure the .env file (see below for example)
- [ ] Set storage folder and all descendants to mode 777 (`chmod +R 777 storage`)
- [ ] Set bootstrap/cache folder and all descendants to mode 777 (`chmod +R 777 bootstrap/cache`)
- [ ] Create a symlink to the project's public folder in the root html directory (`ln -s /var/www/html/legato---line-guides/public ./lg3`)
- [ ] Mount the samba share if not already mounted (`sshfs root@10.1.1.66:/srv/documents/x2/Submissions /srv/submissions -o allow_other`)

### Sample .env file
```
APP_ENV=production
APP_DEBUG=false
APP_KEY=iwRKNF8rOHfNJp0ffZj7n0rw0XIq7cma

DB_HOST=mysql
DB_DATABASE=legato
DB_USERNAME=root
DB_PASSWORD=root

SUBMISSION_DIRECTORY=/srv/submissions/
VERSION=v3.2
```

## Contributors

Role | Name | Company | e-mail
-----|------|---------|-------
Lead Developer | Michael Bourque | AM Fredericks Underwriting Management Ltd. | `michael.bourque@amfredericks.com`
Lead Developer (<v3.0) | Tim Lewis | AM Fredericks Underwriting Management Ltd. | `tim.lewis@amfredericks.com`
Quality Assurance | Rob Russel | AM Fredericks Underwriting Management Ltd. | `rob.russel@amfredericks.com`

<hr />

## Release Notes - LEGATO - Line Guides - Version 3.1

### Bug
<ul>
<li>[<a href='http://dev/jira/browse/LGS-50'>LGS-50</a>] -         Referral Message Bar does not disappear when there is no referral required

</ul>

### Epic
<ul>
<li>[<a href='http://dev/jira/browse/LGS-35'>LGS-35</a>] -         Collection of Bugs, Features, and Issues to be released as V3.1

</ul>

### Improvement
<ul>
<li>[<a href='http://dev/jira/browse/LGS-40'>LGS-40</a>] -         Remove the Submission Number Check from the Save LineGuide Validation

<li>[<a href='http://dev/jira/browse/LGS-52'>LGS-52</a>] -         Increase Percentages to display up to 2 decimal places

</ul>

### IT Help
<ul>
<li>[<a href='http://dev/jira/browse/LGS-41'>LGS-41</a>] -         Save worksheet even if Capacity Errors exist

</ul>

### Story
<ul>
<li>[<a href='http://dev/jira/browse/LGS-18'>LGS-18</a>] -         IAO Code Based Referral Indicator

<li>[<a href='http://dev/jira/browse/LGS-38'>LGS-38</a>] -         Allow users to fill in the form manually if no location information exists

<li>[<a href='http://dev/jira/browse/LGS-39'>LGS-39</a>] -         Reduce Print Footprint

<li>[<a href='http://dev/jira/browse/LGS-42'>LGS-42</a>] -         Allow underwriter to select multiple locations on worksheet (Roll-Up)

<li>[<a href='http://dev/jira/browse/LGS-43'>LGS-43</a>] -         Add a warning to validations when capacity is exceeded

<li>[<a href='http://dev/jira/browse/LGS-44'>LGS-44</a>] -         Provide a default filename for saving LineGuides

<li>[<a href='http://dev/jira/browse/LGS-49'>LGS-49</a>] -         Add IAO Exclusions for InterHanover to Migrations Table / Live Data

</ul>

<hr/>            

## Laravel PHP Framework

[![Build Status](https://travis-ci.org/laravel/framework.svg)](https://travis-ci.org/laravel/framework)
[![Total Downloads](https://poser.pugx.org/laravel/framework/d/total.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/framework/v/stable.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/framework/v/unstable.svg)](https://packagist.org/packages/laravel/framework)
[![License](https://poser.pugx.org/laravel/framework/license.svg)](https://packagist.org/packages/laravel/framework)

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as authentication, routing, sessions, queueing, and caching.

Laravel is accessible, yet powerful, providing powerful tools needed for large, robust applications. A superb inversion of control container, expressive migration system, and tightly integrated unit testing support give you the tools you need to build any application with which you are tasked.

## Official Documentation

Documentation for the framework can be found on the [Laravel website](http://laravel.com/docs).

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](http://laravel.com/docs/contributions).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell at taylor@laravel.com. All security vulnerabilities will be promptly addressed.

### License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)
