$(document).ready(function() {

    // When entering a percentfield, set the value of the 'val' tag
    $('body').on('focusin', '.percentfield', function(event) {
        $(this).data('val', $(this).autoNumeric('get'));
        thisvalue = $(this).parent().next().find('.valuefield');
        thisvalue.data('val', thisvalue.autoNumeric('get'));
    });

    // On specific actions, clear the form
    $('#location').on('change', function(event) {
        refresh();
    });

    $('#otheriao').on('change', function() {
        refresh();
    });

    $('#cleardistribute').on('click', function() {
        refresh();
    });

    // When the 'Auto Distribute' button is clicked perform that function
    $('#distribute').on('click', function() {
        autodistribute();
    });



});
