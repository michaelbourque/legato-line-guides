/* This prototype adds .formatMoney method to Number types
 * It takes a numeric input value and formats it as currency ($ 2,500.00)
 */
Number.prototype.formatMoney = function(c, d, t) {
    var n = this,
        c = isNaN(c = Math.abs(c)) ? 2 : c,
        d = d === undefined ? "." : d,
        t = t === undefined ? "," : t,
        s = n < 0 ? "-" : "",
        i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
        j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};

/*
 *	This function takes an array as a parameter and returns an array with only
 *	the unique values from the input array
 */
function GetUnique(inputArray) {
    var outputArray = [];
    for (var i = 0; i < inputArray.length; i++) {
        if ((jQuery.inArray(inputArray[i], outputArray)) == -1) {
            outputArray.push(inputArray[i]);
        }
    }
    return outputArray;
}

/*
 *	This ajax request passes the form data to limitController to get the Total Capacity available
 *	for the current values
 */
var getTotalCapacity = function(uwlevel, riskgrade, towngrade, buildinggrade, province, iao, other) {

    // create FormData element
    var formData = new FormData();
    formData.append('uwlevel', uwlevel);
    formData.append('riskgrade', riskgrade);
    formData.append('towngrade', towngrade);
    formData.append('buildinggrade', buildinggrade);
    formData.append('province', province);
    formData.append('iao', iao);
    formData.append('other', other);

    // execute the ajax request
    $.ajax({
        url: 'totalcapacity',
        method: 'post',
        dataType: 'json',
        async: false,
        contentType: false,
        processData: false,
        data: formData,
        // on success
        success: function(data) {
            $('.tcv').autoNumeric('set', parseInt(data)); // Display the total capacity
            $('#totalcapacity_field').autoNumeric('set', parseInt(data)); // add the total capacity to the form
            tiv = $('#tiv').autoNumeric('get'); // get the tiv from the form
            // if the tiv is greater than the total capacity
            // Set an error message otherwise, remove the error highlighting
            if (data < tiv) {
                $('#total_capacity').addClass('overline');
                $('#capacity_message').addClass('overline_msg');
                $('#capacity_message').html('<i class="fa fa-exclamation "></i>TIV is Over Capacity by $' + (tiv - data).formatMoney(0, '.', ','));
            } else {
                $('#total_capacity').removeClass('overline');
                $('#capacity_message').removeClass('overline_msg');
                $('#capacity_message').html('');
            }

            // Zero out and clear the result boxes
            $('#totalvalue').autoNumeric('set', 0);
            $('#totalvalue').removeClass('exact');
            $('#totalvalue').removeClass('over');
            $('#totalpercent').autoNumeric('set', 0);
            $('#totalpercent').removeClass('exact');
            $('#totalpercent').removeClass('over');
        }
    });
};

/*
 * 	This ajax request fetches the list of subscribers eligible for the current described Risk
 * 	as well as the prescribed limits for that subscriber for this risk
 *
 * Input Parameters:
 * 		int    uwlevel       - Underwriter Level
 * 		string riskgrade     - Underwriters Risk Grading
 * 		int    towngrade     - FUS Towngrade or approximate
 * 		int    buildinggrade - Rating based on construction type
 * 		string province      - Two character code for Canadian province
 * 		int    iao           - The main iao code associated with the risk (highest tiv)
 * 		string other         - Comma delimited string containing all other iao codes associated with risk
 */
var getSubscribers = function(uwlevel, riskgrade, towngrade, buildinggrade, province, iao, other) {
    $('#subscribersform').empty(); // Clear the subscribers form to make room for the new list

    // create FormData element
    var formData = new FormData();
    formData.append('uwlevel', uwlevel);
    formData.append('riskgrade', riskgrade);
    formData.append('towngrade', towngrade);
    formData.append('buildinggrade', buildinggrade);
    formData.append('province', province);
    formData.append('iao', iao);
    formData.append('other', other);
    $.ajax({
        url: 'getsubscribers',
        method: 'post',
        dataType: 'json',
        async: false,
        contentType: false,
        processData: false,
        data: formData,
        // on successful response
        success: function(data) {
            var element; // predefined for scope
            var keys = []; // predefined for scope
            var knockout = 0; // predefined for scope
            //for each subscriber returned
            $.each(data, function(key, value) {
                // if the subscriber has no available capacity add their key to an array
                if (value.limit === 0) {
                    //$('.' + value.identifier + 'row').remove();
                    keys.push(key);
                }
            });

            // for each key added to the array (0 capacity subscribers)
            $.each(keys, function(key, value) {
                data.splice(value - knockout, 1); // knock it out of the data array
                knockout++; // increment knockout counter for offset
            });

            // for each remaining item in the array (subscribers with capacity)
            $.each(data, function(key, value) {

                $value = value.limit;
                $identifier = value.identifier;
                $subscriber = value.subscriber;

                // Compile the HTML display for the subscriber list
                element = '<div class="row ' + $identifier + 'row">';
                element += '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">';
                element += '<label for="' + $identifier + '">' + value.id + ' - ' + $subscriber + '</label>';
                element += '</div>';
                element += '</div>';
                element += '<div class="row ' + $identifier + 'row">';
                element += '<div class="col-xs-4 col-sm-4 col-md-3 col-lg-3">';
                element += '<input id="' + $identifier + 'percent" name="' + $identifier + 'percent" data-ident="' + $identifier + '" data-val="0" class="form-control percentfield an_percent" placeholder="" value="0" form="lg_form">';
                element += '</div>';
                element += '<div class="col-xs-4 col-sm-4 col-md-5 col-lg-5">';
                element += '<input id="' + $identifier + 'value"  name="' + $identifier + 'value" data-ident="' + $identifier + '" data-val="0" class="form-control valuefield an_currency" placeholder="" value="0" form="lg_form" disabled>';
                element += '</div>';
                element += '<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">';
                element += '<input id="' + $identifier + '"  name="' + $identifier + '"class="form-control capacity an_currency subscribercap" value="' + $value + '" form="lg_form" disabled>';
                element += '</div>';

                $('#subscribersform').append(element); // Push the subscriber list to the view
            });
            initAutoNumeric(); // Initialize autoNumeric classes
        }

    });
};

/*
 *  This ajax request sends the iao codes to the controller and retrieves a list of messages that apply
 */
var getExclusionMessages = function(iao, other) {

    // assemble the FormData element
    var formData = new FormData();
    formData.append('iao', iao);
    formData.append('other', other);
    $.ajax({
        url: 'getmessages',
        method: 'post',
        dataType: 'json',
        async: false,
        contentType: false,
        processData: false,
        data: formData,
        // on successful response
        success: function(data) {
            $('#exclusion_message').html(''); // Clear existing exclusion messages

            // For each message returned
            $.each(data, function(key, value) {
                // if the iao code is not null, post an IAO Code exclusion
                if (value[0] !== null) {
                    $('#exclusion_message').addClass('exclusion_msg');
                    $('#exclusion_message').append('<i class="fa fa-exclamation "></i>' + value[0] + ' for Carrier ' + value[1] + ' (' + (value[2]) + ')<br/>');
                }
                // if the referral value is not null, post a Referral exclusion
                if (value[3] !== null) {
                    $('.referralsig').show();
                    $('#referral_message').addClass('referral_msg');
                    $('#referral_message').append('<i class="fa fa-exclamation "></i>' + value[3] + ' referral required for Carrier ' + value[1] + ' (' + value[2] + ')<br/>');
                }
            });
        }
    });
};

/*
 *	This function validates the line guide for final warnings and alerts on saving
 */
var validate = function() {
    var is_valid = true; // predefined for scope

    // Initialize all booleans to true;
    var valid = {
        subsmission_value: true,
        percent_valid: true,
        tiv_valid: true,
        capacity_limit: true
    };

    // If the total percentage is not 100 (we have over or under subscribed) set a warning
    // otherwise set a Success message
    if ($("#totalpercent").autoNumeric("get") != 100) {
        $("#percent_valid").attr("class", "alert alert-warning");
        $("#percent_valid").html('<i class="fa fa-close"></i> Total coverage must equal 100%');
        // Normally this should fail the validation, but we are passing these so that LG's can be saved for referral
        valid.percent_valid = true;
    } else {
        $("#percent_valid").attr("class", "alert alert-success");
        $("#percent_valid").html('<i class="fa fa-check"></i> Percent');
    }

    // If the tiv is null, blank, or 0 set an error
    // Otherwise, set a Success message
    if ($("#tiv").autoNumeric("get") === "" || $("#tiv").autoNumeric("get") === 0) {
        $("#tiv_valid").attr("class", "alert alert-danger");
        $("#tiv_valid").html('<i class="fa fa-close"></i> TIV cannot be 0');
        //Fail the TIV check
        valid.tiv_valid = false;
    } else {
        $("#tiv_valid").attr("class", "alert alert-success");
        $("#tiv_valid").html('<i class="fa fa-check"></i> TIV');
    }

    // If the tcv is less than the tiv (underinsured) set a warning
    // Otherwise set a success message
    if (parseInt($(".tcv").autoNumeric("get")) < parseInt($("#tiv").autoNumeric("get"))) {
        $("#capacity_valid").attr("class", "alert alert-warning");
        $("#capacity_valid").html('<i class="fa fa-close"></i> Capacity must exceed the TIV');
        // Normally this should fail the validation, but we are passing these so that LG's can be saved for referral
        valid.capacity_limit = true;
    } else {
        $("#capacity_valid").attr("class", "alert alert-success");
        $("#capacity_valid").html('<i class="fa fa-check"></i> Capacity');
    }

    // If any of the checks failed, is_valid is false
    for (var key in valid) {
        if (valid[key] === false) {
            is_valid = false;
            break;
        }
    }

    // If the LG is valid, enable the SAVE button
    // Otherwise, leave it disabled
    if (is_valid === true) {
        $("#save_button").attr("disabled", false);
    } else {
        $("#save_button").attr("disabled", true);
    }
};

/*
 *	This function forces a full refresh of the view and requeries the controller for up-to-the-minute results
 */
var refresh = function() {

    // Clear all capacity/warning/error/and referral messages
    $('#exclusion_message').removeClass('exclusion_msg');
    $('#exclusion_message').html('');

    $('#referral_message').removeClass('referral_msg');
    $('#referral_message').html('');

    $('#overline_message').removeClass('overline_msg');
    $('#overline_message').html('');

    $('#error_message').removeClass('error_msg');
    $('#error_message').html('');

    $('#warning_message').removeClass('warning_msg');
    $('#warning_message').html('');


    // if the location is null, set location count to 0
    // if there is no submission, only one location can be included
    // otherwise, the location count is the number of locations in the view
    if ($('#location').val() === null) {
        $locationCount = 0;
    } else if ($('#nosub').val() === 'nosub') {
        $locationCount = 1;
    } else {
        $locationCount = $('#location').val().length;
    }

    // Set up variables
    var locations = data.locations; // locations data (retrieved from data)
    var $location = null; // location is null
    var chosenLocations = ($('#location').val()); // a list of chosenLocations (array) from the selectpicker
    var newTIV = 0; // initialize a new TIV
    var highTIV = 0; // initialize a container for the highest TIV
    var mainIAO = ''; // initialize a container for the main IAO
    var provinces = []; // initialize an array for location provinces
    var mainiaoval = '';
    var otheriaoval = '';
    var iaoCode = ''; // Predefined for scope
    var otherCode = ''; // Predefined for scope

    // Single Location
    if ($locationCount == 1) {
        // Blank Form
        if ($('#nosub').val() === 'nosub') {
            // copy existing IAO values Because the user input them and we cannot recover them from a query
            mainiaoval = $('#iao').val();
            otheriaoval = $('#otheriao').val();
        }

        // Clear the form values for IAO and Other
        $('#iao').val('');
        $('#otheriao').empty();

        // Not a Blank Form
        if ($('#nosub').val() !== 'nosub') {
            //Refresh the selectpicker (no values)
            $('#otheriao').selectpicker('refresh');
        }

        // Clear any IAO Exclusion messages
        $('#exclusion_message').removeClass('exclusion_msg');
        $('#exclusion_message').html('');

        // Not a Blank Form
        if ($('#nosub').val() !== 'nosub') {

            // For each location
            $.each(locations, function(index, value) {
                // If the location matches the chosen one
                // Set thislocation to it's value
                if (value.location_number == $('#location').val()) {
                    thislocation = value;
                }
            });

            // Set iaoCode to the industry code of this location
            iaoCode = thislocation.industry_code;

            // Set TIV and Province to the values associated with this location
            $('#tiv').autoNumeric('set', thislocation.tiv);
            $('#province').val(thislocation.province);

            // Update the IAO Code on the form
            $('#iao').val((iaoCode));

            // Collectg other_code
            iaoCode = thislocation.other_code;

            // If there are currently no other iao codes
            if (($("#otheriao option[value='" + iaoCode + "']").length === 0)) {
                // and the iao code is not null, blank, or 0000
                if ((iaoCode !== '') && (iaoCode !== '0000')) {
                    // append the iaoCode to the list.
                    $('#otheriao').append('<option value="' + (iaoCode) + '" selected>' + (iaoCode) + '</option>');
                    // refresh the selectpicker for other iao's
                    $('#otheriao').selectpicker('refresh');
                }
            }

            // if other_codes are associated with this location, add them to morecodes
            // this usually occurs when there are multiple buildings rolled up to one Location
            // The other codes come from the other_iao selectpicker
            morecodes = thislocation.other_codes;

            // if morecodes is not 'undefined'
            if (morecodes) {
                // for each additional code
                $.each(morecodes, function(index, value) {
                    // add the values to the selectpicker and refresh it
                    $('#otheriao').append('<option value="' + (value) + '" selected>' + (value) + '</option>');
                    $('#otheriao').selectpicker('refresh');
                });
            }

            // update the form with the new values
            $('#constructiongrade').val(thislocation.construction_code);
            $('#towngrade').val(thislocation.town_grade);
            $('#province').val(thislocation.province);
            $('#iao').val((thislocation.industry_code));
        } else {
            // If this is a blank form, just replace the iao codes entered by the user
            $('#iao').val(mainiaoval);
            $('#otheriao').val(otheriaoval);
        }

        // Get the total capacity based on parameters
        getTotalCapacity(
            $('#uwclass').val(),
            $('#riskgrading').val(),
            $('#towngrade').val(),
            $('#constructiongrade').val(),
            $('#province').val(),
            $('#iao').val(),
            $('#otheriao').val()
        );
        // Get a list of subscribers and their capacities
        getSubscribers(
            $('#uwclass').val(),
            $('#riskgrading').val(),
            $('#towngrade').val(),
            $('#constructiongrade').val(),
            $('#province').val(),
            $('#iao').val(),
            $('#otheriao').val()
        );
        // Get any exclusion messages that apply
        getExclusionMessages(
            $('#iao').val(),
            $('#otheriao').val()
        );

        // If more than 1 location
    } else if ($locationCount > 1) {
        // clear any exclusion messages
        $('#exclusion_message').removeClass('exclusion_msg');
        $('#exclusion_message').html('');

        // if the variable chosenLocations exists and is an array
        if ($.isArray(chosenLocations)) {
            // for each element in that array
            $.each(chosenLocations, function(index, value) {
                // add the location province to the provinces array
                // Note that the Offset is -1 because Location 1 = element 0
                provinces.push(locations[value - 1].province);

                // This following structure is intended to populate the form with the worst case scenario
                // values from all locations being considered.

                // If the location town_grade is greater than the current town-grade setting
                if (locations[value - 1].town_grade > $('#towngrade').val()) {
                    // change the towngrade
                    $('#towngrade').val(locations[value - 1].town_grade);
                }

                // If the location construction_code is greater than the current construction grade setting
                if (locations[value - 1].construction_code > $('#constructiongrade').val()) {
                    // change the construction grade
                    $('#constructiongrade').val(locations[value - 1].construction_code);
                }

                // This structure ensures we use the iao code of the highest value location as the MAIN IAO CODE
                // if the location tiv is greater than the value of highTIV
                if (locations[value - 1].tiv > highTIV) {
                    // update highTIV
                    highTIV = locations[value - 1].tiv;
                    // get the IAO code
                    mainIAO = locations[value - 1].industry_code;
                }

                // add the location TIV to the totalTIV being stored in newTIV
                newTIV = parseInt(newTIV) + parseInt(locations[value - 1].tiv);

                // Get the IAO codes for the current location
                otherCode = locations[value - 1].other_code;
                iaoCode = locations[value - 1].industry_code;

                // if the shown iao is different than the location's iaoCode
                if (($('#iao').val() !== iaoCode)) {
                    // if the iao code is not in the otheriao list
                    if (($("#otheriao option[value='" + (iaoCode) + "']").length === 0)) {
                        // if the iaocode is not null, blank, or 0000
                        if ((iaoCode !== '') && (iaoCode !== '0000')) {
                            // append the iaoCode to the otherIAOCode list
                            $('#otheriao').append('<option value="' + (iaoCode) + '" selected>' + (iaoCode) + '</option>');
                            // refresh the iaocode selectpicker
                            $('#otheriao').selectpicker('refresh');
                        }
                    }
                }

                // if the othercode is not in the otheriao list
                if (($("#otheriao option[value='" + (otherCode) + "']").length === 0)) {
                    // if the othercode is not null, blank, or 0000
                    if ((otherCode !== '') && (otherCode !== '0000')) {
                        // append the othercode to the othercode selectpicker
                        $('#otheriao').append('<option value="' + (otherCode) + '" selected>' + (otherCode) + '</option>');
                    }
                }

                // update the otheriaocode selectpicker
                $('#otheriao').selectpicker('refresh');

                // if other_codes are associated with this location, add them to morecodes
                // this usually occurs when there are multiple buildings rolled up to one Location
                // The other codes come from the other_iao selectpicker
                morecodes = locations[value - 1].other_codes;

                // if morecodes is not undefined
                if (morecodes) {
                    // for each code in morecodes
                    $.each(morecodes, function(index, value) {
                        // if the morecode is not in the otheriao list
                        if (($("#otheriao option[value='" + (value) + "']").length === 0)) {
                            // add the morecode
                            $('#otheriao').append('<option value="' + (value) + '" selected>' + (value) + '</option>');
                            // refresh the otheriaocode selectpicker
                            $('#otheriao').selectpicker('refresh');
                        }
                    });
                }
            });
        }
        // drop all non-unique provinces from the provinces array
        provinces = GetUnique(provinces);

        // If the number of provinces for chosen locations is more than 1
        // produce a warning messages
        // otherwise, clear any existing province warning messages
        if (provinces.length > 1) {
            $('#province_warning').modal('show');
            $('#province').val('Multiple');
            $('#warning_message').html('<i class="fa fa-exclamation "></i>Multiple Provinces Selected');
            $('#warning_message').addClass('warning_msg');
        } else {
            $('#province').val(provinces[0]);
            $('#warning_message').html('');
            $('#warning_message').removeClass('warning_msg');
        }
        // if the mainIAO code is in the otherIAO code list
        if (($("#otheriao option[value=" + (mainIAO) + "]").length > 0)) {
            // remove the mainIAO code from the otherIAO code selectpicker
            $("#otheriao option[value=" + (mainIAO) + "]").remove();
            // and refresh the selectpicker
            $('#otheriao').selectpicker('refresh');
        }
        // Set the tiv to the new totalled tiv
        $('#tiv').autoNumeric('set', parseInt(newTIV));
        // set the mainIAO code to the display
        $('#iao').val((mainIAO));

        // Get the total capacity based on parameters
        getTotalCapacity(
            $('#uwclass').val(),
            $('#riskgrading').val(),
            $('#towngrade').val(),
            $('#constructiongrade').val(),
            $('#province').val(),
            $('#iao').val(),
            $('#otheriao').val()
        );
        // Get a list of subscribers and their capacities
        getSubscribers(
            $('#uwclass').val(),
            $('#riskgrading').val(),
            $('#towngrade').val(),
            $('#constructiongrade').val(),
            $('#province').val(),
            $('#iao').val(),
            $('#otheriao').val()
        );
        // Get any exclusion messages that apply
        getExclusionMessages(
            $('#iao').val(),
            $('#otheriao').val()
        );
        // If there are no locations
    } else if ($locationCount === 0) {
        // Blank everything
        $('#towngrade').val('');
        $('#constructiongrade').val('');
        $('#province').val('');
        $('#tiv').autoNumeric('set', 0);
        $('#iao').val('');
        $('#otheriao').empty();
        $('#otheriao').selectpicker('refresh');

        // Get the total capacity based on parameters
        getTotalCapacity(
            $('#uwclass').val(),
            $('#riskgrading').val(),
            $('#towngrade').val(),
            $('#constructiongrade').val(),
            $('#province').val(),
            $('#iao').val(),
            $('#otheriao').val()
        );
        // Get a list of subscribers and their capacities
        getSubscribers(
            $('#uwclass').val(),
            $('#riskgrading').val(),
            $('#towngrade').val(),
            $('#constructiongrade').val(),
            $('#province').val(),
            $('#iao').val(),
            $('#otheriao').val()
        );
    }
};

// This function updates the display based on currently visible Values
// It is executed whenever a form value is changed, or if the user changes the focusin
// away from an active form control.
var update = function() {
    var percentstack = []; // predefined for scope;
    var percents = $('[id*="percent"]').not('#totalpercent_field, #totalpercent, #percent_valid'); // jquery to get the subscriber percent fields only

    // For each percent (subscriber)
    $.each(percents, function(index, value) {
        // add the subscriber and the percentage to percentstack
        percentstack.push([value.id, value.value]);
    });

    // clear exlusion messages
    $('#exclusion_message').removeClass('exclusion_msg');
    $('#exclusion_message').html('');

    // clear referral messages
    $('#referral_message').removeClass('referral_msg');
    $('#referral_message').html('');

    // Pass the form values to java variables
    var uwclass = $('#uwclass').val();
    var riskgrading = $('#riskgrading').val();
    var towngrade = $('#towngrade').val();
    var constructiongrade = $('#constructiongrade').val();
    var province = $('#province').val();
    var iao = $('#iao').val();
    var otheriao = $('#otheriao').val();
    var tiv = $('#tiv').autoNumeric('get');

    // if the tiv is non-zero
    if (tiv > 0) {

        // clear any error messages
        $('#capacity_message').removeClass('error_msg');
        $('#capacity_message').html('');

        // refetch the Total Capacity
        getTotalCapacity(
            uwclass,
            riskgrading,
            towngrade,
            constructiongrade,
            province,
            iao,
            otheriao
        );

        // refecth the Subscriber List
        getSubscribers(
            uwclass,
            riskgrading,
            towngrade,
            constructiongrade,
            province,
            iao,
            otheriao
        );

        // Start the preservation routine for saving Percents/Values in Subscribers Form
        tiv = parseFloat($('#tiv').autoNumeric('get'));

        // for each element in percentstack
        $.each(percentstack, function(index, value) {
            // if the ident is not null
            if ($('#' + value[0]).length > 0) {
                // set the ident value to the percentage
                $('#' + value[0]).val(value[1]);
                // get thisIdent from the proper data tag
                thisIdent = $('#' + value[0]).data('ident');
                // calculate the value for thisIdent
                thisValue = (tiv * (parseFloat(value[1]) / 100));
                // display the calculated value
                $('#' + thisIdent + 'value').autoNumeric('set', thisValue);
            }
        });

        // update the totalPercent
        totalpercent = parseFloat($('#totalpercent').autoNumeric('get'));
        $.each(percentstack, function(index, value) {
            totalpercent += parseFloat(value[1]);
        });
        if (totalpercent >= 100) {
            totalpercent = parseInt(totalpercent);
        }

        $('#totalpercent').autoNumeric('set', totalpercent);
        totalvalue = parseInt((tiv * (totalpercent / 100)));
        $('#totalvalue').autoNumeric('set', totalvalue);
        updatePercents(percentstack);

        // End the preservation routine

        // get any Exclusion Messages
        getExclusionMessages(
            iao,
            otheriao
        );
        // show the subscriber panel
        $('#subscriberspanel').css('visibility', 'visible');
        // if there is no TIV value
    } else {
        // throw an error message
        $('#capacity_message').addClass('error_msg');
        $('#capacity_message').html('<i class="fa fa-exclamation-circle"></i>You must set a TIV for this location');
    }
    // update the button status (Print / Save)
    checkButtons();
};


/*
 *	This function locks and unlocks the Print and Save buttons
 *	based on the required values
 */
var checkButtons = function() {
    var locked = false;
    if ($('#submission').val() === '') {
        locked = true;
    }
    if ($('#location').val() === '') {
        locked = true;
    }
    if ($('#underwriter').val() === '') {
        locked = true;
    }
    if ($('#towngrade').val() === '') {
        locked = true;
    }
    if ($('#constructiongrade').val() === '') {
        locked = true;
    }
    if ($('#riskgrading').val() === '') {
        locked = true;
    }
    if ($('#province').val() === '') {
        locked = true;
    }
    if ($('#tiv').val() === '') {
        locked = true;
    }
    if ($('#iao').val() === '') {
        locked = true;
    }
    if (locked) {
        $('#save').addClass('disabled');
        $('#save').parent().addClass('disabled');
        $('#print').addClass('disabled');
        $('#print').parent().addClass('disabled');
    } else {
        $('#save').removeClass('disabled');
        $('#save').parent().removeClass('disabled');
        $('#print').removeClass('disabled');
        $('#print').parent().removeClass('disabled');
    }
};

/*
 *	This function updates the percentages and the display after the Total Percent is updated
 */
var updatePercents = function(percentstack) {
    // get the total capacity value
    tcv = $('.tcv').autoNumeric('get');

    // if totalvalue > tiv throw overage message
    if (totalvalue > tiv) {
        $('#totalvalue').removeClass('exact');
        $('#totalvalue').addClass('over');
        // if totalvalue is the same as tiv, throw exact message
    } else if (totalvalue == tiv) {
        $('#totalvalue').removeClass('over');
        $('#totalvalue').addClass('exact');
        // if totalvalue > tcv throw overage message
    } else if (totalvalue > tcv) {
        $('#totalvalue').addClass('over');
        // otherwise just clear over and exact messages
    } else {
        $('#totalvalue').removeClass('over');
        $('#totalvalue').removeClass('exact');
    }

    // if total percent exceeds 100, throw overage message
    if (totalpercent > 100) {
        $('#totalpercent').removeClass('exact');
        $('#totalpercent').addClass('over');
        // if total percent is 100, throw exact messasge
    } else if (totalpercent == 100) {
        $('#totalpercent').removeClass('over');
        $('#totalpercent').addClass('exact');
        // otherwise just clear over and exact messages
    } else {
        $('#totalpercent').removeClass('over');
        $('#totalpercent').removeClass('exact');
    }

    // for each value in the percentstack
    $.each(percentstack, function(index, value) {
        // nullify ident
        ident = null;

        // if an element for the precent stack value exists, get the 'ident' value
        if ($('#' + value[0]).length > 0) {
            ident = $('#' + value[0]).data('ident');
        }

        // if ident if not null
        if (ident !== null) {
            // set a target for the ident
            target = '#' + ident + 'value';
            // get the subscriber cap for the ident
            subscribercap = parseInt($('#' + ident).autoNumeric('get'));

            // if the value of the target exceeds the subscriber cap
            // throw overage message and remove limit message
            if ($(target).autoNumeric('get') > subscribercap) {
                $(this).addClass('over');
                $(target).addClass('over');
                $(this).removeClass('limit');
                $(target).removeClass('limit');
                // if the value of the target matches the subscribercap
                // thow limit message and remove overage messages
            } else if ($(target).autoNumeric('get') == subscribercap) {
                $(this).addClass('limit');
                $(target).addClass('limit');
                $(this).removeClass('over');
                $(target).removeClass('over');
                // otherwise just remove overage and limit messages
            } else {
                $(this).removeClass('over');
                $(target).removeClass('over');
                $(this).removeClass('limit');
                $(target).removeClass('limit');
            }
        }
    });

    // updatge the display with the new percents and values
    $('#totalvalue_field').autoNumeric('set', parseInt($('#totalvalue').autoNumeric('get')));
    $('#totalpercent_field').autoNumeric('set', parseFloat($('#totalpercent').autoNumeric('get')));
};

/*
 * 	This function automatically assigns capacity to each subscriber with a bias on those that appear
 * 	sooner in the list of subscribers.
 */
var autodistribute = function() {

    var thiscap = ''; // predefined for scope;
    var totalpercent = $('#totalpercent').autoNumeric('get'); // get the totalpercent
    var availablepercent = 100.00; // set the availablepercent to 100
    var caps = $('.subscribercap'); // get the subscriber caps as an array
    var tiv = $('#tiv').autoNumeric('get'); // get the tiv for the current location(s)

    // for each subscriber with capacity
    $.each(caps, function(index, value) {

        // if there is available percent and we are not yet at 100 percent
        if ((availablepercent >= 0) && (totalpercent < 100)) {
            thiscap = value.value.replace(/\D/g, ''); // clean up the string to remove non digits

            // if this capacity is greater than the tiv
            // assign the whole amount to this subscriber
            if (parseInt(thiscap) > parseInt(tiv)) {
                $('#' + value.id + 'percent').autoNumeric('set', availablepercent);
                $('#' + value.id + 'value').autoNumeric('set', availablepercent * tiv / 100);
                availablepercent = 0;
                // if it isn't and we have availablepercent left
            } else if (availablepercent > 0) {

                // thispercent is the integer floor of the converted float / 100 to produce a percentage
                thispercent = Math.floor(thiscap / tiv * 100 * 100) / 100;

                // if we can increase the percentage slightly and still be below the cap, then lets do so.
                // NOTE this seems like a less than optimal way to do this, but it works and compensates for the inaccruacy of float to integer precision
                if (((thispercent + 0.01) * tiv / 100) <= thiscap) {
                    thispercent = thispercent + 0.01;
                }
                // if thispercent exceeds the availablepercent, dump it all
                if (thispercent > availablepercent) {
                    $('#' + value.id + 'percent').autoNumeric('set', availablepercent);
                    $('#' + value.id + 'value').autoNumeric('set', availablepercent * tiv / 100);
                    availablepercent = 0;
                    // otherwise, add this percent and decrease it from available percent.
                } else {
                    $('#' + value.id + 'percent').autoNumeric('set', thispercent);
                    $('#' + value.id + 'value').autoNumeric('set', thispercent * tiv / 100);
                    availablepercent = availablepercent - thispercent;
                }
            }
        }
    });
    // update the screen;
    update();
};
