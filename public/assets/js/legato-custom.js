var init; // Global variable to allow init function to be accessed
var initAutoNumeric; // Global variable to allow AutoNumeric definitions to be initialized
var tableChanged = false;

$(document).ready(function() {

    // Requires jQuery!
    jQuery.ajax({
                url: "http://dev/jira/s/3c3ad574684d56ef49014193aa15e53e-T/en_US-ue6lxm/64022/42/1.4.26/_/download/batch/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector-embededjs/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector-embededjs.js?locale=en-US&collectorId=7cf47ac7",
            type: "get",
        cache: true,
        dataType: "script"
    });

    initAutoNumeric = function() {
        /*
            Initialize a class for individual percentages using autoNumeric
         */
        $(".an_percent").autoNumeric('init', {
            aSep: '',
            aSign: '%',
            lZero: 'deny',
            pSign: 's',
            vMax: '100',
            mDec: '2',
            aPad: false
        });
        /*
            Initialize a class for total percent using autoNumeric
            TODO This is now redundant. Remove this at a later date.
         */
        $(".an_percenttotal").autoNumeric('init', {
            aSep: '',
            aSign: '%',
            lZero: 'deny',
            pSign: 's',
            mDec: '2',
            aPad: false
        });
        /*
            Initialize a class for currency using autoNumeric
         */
        $(".an_currency").autoNumeric('init', {
            aSign: '$',
            lZero: 'deny',
            mDec: '0',
            dGroup: '3',
            aSep: ','
        });
    };

    init = function(data) {
        initAutoNumeric();

        // Reset Capacity Field to standard numeric in case of refresh (interferes with autonumeric('set'))
        $('#totalcapacity_field').val($('#totalcapacity_field').val().replace(/\D/g, ''));

        // Predfine variables for scope
        var locations = null;
        var $location = null;

        // If locations are defined, populate locations
        if (typeof data.locations != 'undefined') {
            locations = data.locations;
        }

        // If there is a single policy, populate the policy field in the form and disable the control
        if (data.policycount == 1) {
            $policy = data.policies[0];
            $('#policy').val($policy);
            $('#policy').prop('disabled', true);
        }

        // If there is a single location, populate the form fields and disable the appropriate controls
        if (data.locationcount == 1) {
            $location = data.locations[0];
            $('#constructiongrade').val($location.construction_code);
            $('#constructiongrade').prop('disabled', true);
            $('#towngrade').val($location.town_grade);
            $('#province').val($location.province);
            $('#iao').val(($location.industry_code));
            if (($location.other_code !== '0000') && ($location.other_code !== $location.industry_code)) {
                $('#otheriao').val($location.other_code);
                $('#otheriao').selectpicker('selectAll');
                $('#otheriao').selectpicker('render');
            }
            // retrieve the tiv for the single location and populate the tiv field in the form
            thistiv = parseInt($location.tiv);
            $('#tiv').val(thistiv);
            $('#tiv').prop('disabled', true); // Disabled the control
        }

        // If locations exist
        if (locations !== null) {
            // for each location
            $.each(locations, function(index, value) {
                // if the location number matches the location in the form
                // set thislocation to it's value
                if (value.location_number == $('#location').val()) {
                    thislocation = value;
                }
            });

            // populate the remaining form elements with thislocation details
            $('#constructiongrade').val(thislocation.construction_code);
            $('#towngrade').val(thislocation.town_grade);
            $('#province').val(thislocation.province);
            $('#iao').val((thislocation.industry_code));
            $('#otheriao').val(thislocation.other_code);
            $('#otheriao').selectpicker('selectAll');
            $('#otheriao').selectpicker('render');
            thistiv = parseInt(thislocation.tiv);
            $('#tiv').autoNumeric('set', thistiv);

        }

        // Get the total capacity based on parameters
        getTotalCapacity(
            $('#uwclass').val(),
            $('#riskgrading').val(),
            $('#towngrade').val(),
            $('#constructiongrade').val(),
            $('#province').val(),
            $('#iao').val(),
            $('#otheriao').val()
        );

        // Get a list of subscribers and their capacities
        getSubscribers(
            $('#uwclass').val(),
            $('#riskgrading').val(),
            $('#towngrade').val(),
            $('#constructiongrade').val(),
            $('#province').val(),
            $('#iao').val(),
            $('#otheriao').val()
        );

        // Get any exclusion messages that apply
        getExclusionMessages(
            $('#iao').val(),
            $('#otheriao').val()
        );

        // Print the window when the 'Print' button is pressed
        $("#print").on("click", function(e) {
            e.preventDefault();
            cells = $('[id^="cell"]');
            $.each(cells, function(index, value) {
                if ($(value).val() !== '') {
                    tableChanged = true;
                }
            });
            if ((tableChanged === false) && ($('#notes').val() === '')) {
                $('#notes').addClass('hidden-print');
                $('#excel_table').addClass('hidden-print');
            } else {
                $('#notes').removeClass('hidden-print');
                $('#excel_table').removeClass('hidden-print');
            }

            window.print();
        });

        // When the 'Save' button is pressed (on Main Window)
        $("#save").on("click", function(e) {
            e.preventDefault();
            validate(); // Validate the form data
            var filenamehint = "LG" + $("#submission").val(); // set the filenamehint to include the submission number
            $('#filenamehint').html(filenamehint);
            $("#save_modal").modal("show"); // show the Save modal
        });

        // When the 'Save' button is pressed (on Save Modal)
        $("#save_button").on("click", function(e) {
            var dateTag = new Date(); // Get the instantaneous date/time stamp
            thisDate = dateTag.toISOString().replace(/:/g, "-"); // Format the date/time stamp to ISO 8601
            thisDate = thisDate.substring(0, thisDate.length - 5); // Drop the .###Z from the time stamp
            var filename_prefix = thisDate + "LG" + $("#submission").val(); // Set the filename prefix
            $('#filenamehint').html(filename_prefix); // update the filenamehint to match the prefix
            $(":input").prop("disabled", false); // enable the input
            // If the user has entered a value
            if ($('#filenametag').val() !== '') {
                filename = '-' + $('#filenametag').val(); // append it to the filename
            } else {
                filename = ''; // otherwise, make filename empty
            }
            filename_suffix = ".pdf"; // define the filename suffic (extension)
            fullfile = filename_prefix + filename + filename_suffix; // Concatenate the components to form the full file name
            $("#fn_full").val(fullfile); // display the full filename
            $("#capacitymessages").val($('#capacity_message').html()); // Add the Capacity Messages to the form
            $("#exclusionmessages").val($('#exclusion_message').html()); // Add the Exclusion Messages to the form
            $("#warningmessages").val($('#warning_message').html()); // Add the Warning Messages to the form
            $("#lg_form").submit(); // Submit the data
        });

        // When the user exits the submission field, validate that it is between 1000 and 999999
        // If so, enable the 'SAVE' button
        // This is required because the submission number is necessary for saving the file to the
        // correct folder
        $('#submission').on('focusout', function() {
            if (($('#submission').val() < 999999) && ($('#submission').val() > 1000)) {
                $('#save').parent().removeClass('disabled');
            }
        });

        // If the underwriter is changed, update the uwclass
        $('#underwriter').on("change", function(e) {
            $('#uwclass').val($(this).find(":selected").data('uwclass'));
        });

        // Any time a field is exited or changed, update the form
        $('#submission').on('focusout', function() {
            update();
        });
        $('#location').on('focusout', function() {
            update();
        });
        $('#underwriter').on('change', function() {
            update();
        });
        $('#towngrade').on('change', function() {
            update();
        });
        $('#constructiongrade').on('change', function() {
            update();
        });
        $('#riskgrading').on('change', function() {
            update();
        });
        $('#province').on('change', function() {
            update();
        });
        $('#tiv').on('focusout', function() {
            update();
        });
        $('#iao').on('focusout', function() {
            update();
        });
        $('#otheriao').on('focusout', function() {
            update();
        });

        // When a percentfield is entered, preserve the percent by adding a data tag
        // Also preserve the value by adding a data tag to the value element
        $('body').on('focusin', '.percentfield', function(event) {
            $(this).data('val', $(this).autoNumeric('get'));
            thisvalue = $(this).parent().next().find('.valuefield');
            thisvalue.data('val', thisvalue.autoNumeric('get'));
        });

        // Any time a percentfield is modified
        $('body').on('change', '.percentfield', function(event) {
            percentstack = []; // Array to hold existing idents and percentages
            thisvaluefield = $(this).parent().next().find('.valuefield'); // find the value field associated with the

            // Store the precent and value
            prevperc = $(this).data('val');
            prevval = thisvaluefield.data('val');

            newpercent = $(this).autoNumeric('get'); // get the new percentage
            tiv = $('#tiv').autoNumeric('get'); // get the tiv
            newvalue = (tiv * (newpercent / 100)); // calculate the new value
            ident = $(this).data('ident'); // get the ident (subscriber)
            target = '#' + ident + 'value'; // set the target
            $(target).autoNumeric('set', newvalue); // update the target

            // Update Total Percent
            totalpercent = parseFloat($('#totalpercent').autoNumeric('get'));
            totalpercent = parseFloat(totalpercent) - parseFloat(prevperc) + parseFloat(newpercent);
            totalpercent = parseFloat(totalpercent);
            $('#totalpercent').autoNumeric('set', parseFloat(totalpercent));

            // Update Total Value
            totalvalue = parseFloat($('#totalvalue').autoNumeric('get'));
            totalvalue = parseFloat(totalvalue) - parseFloat(prevval) + parseFloat(newvalue);
            totalvalue = parseFloat(totalvalue);
            $('#totalvalue').autoNumeric('set', parseFloat(totalvalue));

            // Collect relevant values
            thisid = $(this).attr('id');
            thisvalue = $(this).val();

            update(); // Update the form
        });
    };
});
