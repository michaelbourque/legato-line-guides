/*  These functions are used to manipulate the table worksheet provided at the bottom of the LineGuides Form
 *	The functions are self explanatory: addRow, removeRow, addCol, removeCol
 * 	Each function manipulates HTML tables to provide a visual table
 */

var rows = 1;
var cols = 1;

function addRow() {
    rows++;
    var string = "";
    string += '<tr class="tbl_row">';
    for (var i = 0; i < cols; i++) {
        string += '<td class="tbl_cell"><input type="text" id="cell[' + rows + '][' + (i + 1) + ']" name="cell[' + rows + '][' + (i + 1) + ']" class="form-control" form="lg_form"/></td>';
    }
    string += '</tr>';
    $("#excel_table").append(string);
}

function removeRow() {
    if (rows > 1) {
        $("#excel_table tr:last").remove();
        rows--;
    }
}

function addCol() {
    cols++;
    var counter = 1;
    $(".tbl_row").each(function() {
        $(this).append('<td class="tbl_cell"><input type="text" id="cell[' + counter + '][' + cols + ']" name="cell[' + counter + '][' + cols + ']" class="form-control" form="lg_form"/></td>');
        counter++;
    });
}

function removeCol() {
    if (cols > 1) {
        $(".tbl_row").each(function() {
            $(this).find("td:last").remove();
        });
        cols--;
    }
}
